<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mymodel','mm');
	}

	public function index()
	{
		if($this->session->userdata('user'))
		{
			redirect('home');
		}
		else
		{
			$this->load->view('admin/login');
			
		}
	}

	public function admin()
	{
		if(!$this->session->userdata('user'))
		{
			$this->load->view('admin/login');
		}
		else
		{
			$this->load->view('admin/index');
		}
	}

	

	public function login()
	{
		$email= $this->input->post('email');
		$pass = $this->input->post('password');

		$login= $this->mm->login($email,$pass);
			if($login)
			{
				die();
				$session= $this->session->set_userdata('user',$login);
				redirect('home');
			}
			else
			{
				echo $this->session->set_flashdata('msg','"User Name or Password is Wrong"');
				redirect('login');
			}
	}

	public function home()
	{
		if(!$this->session->userdata('user'))		// here admin is session name....
			{
					redirect('login');		// here admin is Login page name which is for Admin...
			}
			else
			{
				$this->load->view('admin/headerlinks');
				$this->load->view('admin/index');
			}
	}

	

	public function logout()
	{
		$this->session->unset_userdata('user');
		redirect('login');
	}

	public function change_pass()
	{
		if($this->session->userdata('user'))
		{
			$this->load->view('admin/change_pass');	
		}
		else
		{
			$this->load->view('admin/login');
		}
		
	}

	public function update_pass()
	{
	 $session_id=$this->session->userdata('user');
	 $password = $this->input->post('password');

		
		$update=$this->mm->update_pass($session_id,$password);
			if($update)
			{
				$this->session->unset_userdata('user');
				echo $this->session->set_flashdata('msg','"Password Updated Successfuly"');
				redirect('login');
			}
	}


// ============================================
	public function category_1()
	{
		if($this->session->userdata('user'))
		{
		$rec['data1']=$this->mm->rec1();
		$this->load->view('admin/headerlinks');
		$this->load->view('admin/category_1',$rec);	
		}
		else
		{
			redirect('login');
		}
	}

	public function insert1()
	{
		 $data1 = array(
			'Name' => $this->input->post('name1')
		);
		$insert1 = $this->mm->insert1($data1);
		if($insert1)
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_1');
		}
		else
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_1');

		}

	}

	public function delete1($id)
	{
		$deleteUser=$this->mm->delete1($id);
				if($deleteUser)
				{
					echo $this->session->set_flashdata('msg','"User deleted Successfuly"');
					redirect('category_1');
				}
	}

	public function edit1($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit['data']=$this->mm->edit1($id);
		$this->load->view('admin/editUser',$edit);
		}
		else
		{
			redirect('login');
		}
	}

	public function update1()
	{
		$id=$this->input->post('id');
		$name=$this->input->post('name');
		$update=$this->mm->update1($name,$id);
			if($update)
			{
				echo $this->session->set_flashdata('msg','"User Updated Successfuly"');
				redirect('category_1');
			}
	}

	// ====================Category -2
	public function category_2()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$rec['data']=$this->mm->rec2();
		$this->load->view('admin/category_2',$rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function insert2()
	{
		 $data = array(
			'Name' => $this->input->post('name')
		);
		$insert = $this->mm->insert2($data);
		if($insert)
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_2');
		}
		else
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_2');

		}

	}

	public function delete2($id)
	{
		$deleteUser=$this->mm->delete2($id);
				if($deleteUser)
				{
					echo $this->session->set_flashdata('msg','"User deleted Successfuly"');
					redirect('category_2');
				}
	}

	public function edit2($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit['data']=$this->mm->edit2($id);
		$this->load->view('admin/editUser2',$edit);
		}
		else
		{
			redirect('login');
		}
	}

	public function update2()
	{
		$id=$this->input->post('id');
		$name=$this->input->post('name');
		
		$update2=$this->mm->update2($name,$id);
			if($update2)
			{
				echo $this->session->set_flashdata('msg','"User Updated Successfuly"');
				redirect('category_2');
			}
	}


	// ====================Category -3
	public function category_3()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$rec['data']=$this->mm->rec3();
		$this->load->view('admin/category_3',$rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function insert3()
	{
		 $data = array(
			'Name' => $this->input->post('name')
		);
		$insert = $this->mm->insert3($data);
		if($insert)
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_3');
		}
		else
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_3');

		}

	}
	
	public function delete3($id)
	{
		$deleteUser=$this->mm->delete3($id);
				if($deleteUser)
				{
					echo $this->session->set_flashdata('msg','"User deleted Successfuly"');
					redirect('category_3');
				}
	}

	public function edit3($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit['data']=$this->mm->edit3($id);
		$this->load->view('admin/editUser3',$edit);
		}
		else
		{
			redirect('login');
		}
	}

	public function update3()
	{
		$id=$this->input->post('id');
		$name=$this->input->post('name');
		
		$update3=$this->mm->update3($name,$id);
			if($update3)
			{
				echo $this->session->set_flashdata('msg','"User Updated Successfuly"');
				redirect('category_3');
			}
	}


// ====================Category -4
	public function category_4()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$rec['data']=$this->mm->rec4();
		$this->load->view('admin/category_4',$rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function insert4()
	{
		 $data = array(
			'Name' => $this->input->post('name')
		);
		$insert = $this->mm->insert4($data);
		if($insert)
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_4');
		}
		else
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_4');

		}

	}
	
	public function delete4($id)
	{
		$deleteUser=$this->mm->delete4($id);
				if($deleteUser)
				{
					echo $this->session->set_flashdata('msg','"User deleted Successfuly"');
					redirect('category_4');
				}
	}

	public function edit4($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit['data']=$this->mm->edit4($id);
		$this->load->view('admin/editUser4',$edit);
		}
		else
		{
			redirect('login');
		}
	}

	public function update4()
	{
		$id=$this->input->post('id');
		$name=$this->input->post('name');
		
		$update4=$this->mm->update4($name,$id);
			if($update4)
			{
				echo $this->session->set_flashdata('msg','"User Updated Successfuly"');
				redirect('category_4');
			}
	}



	// ====================Category -5
	public function category_5()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$rec['data']=$this->mm->rec5();
		$this->load->view('admin/category_5',$rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function insert5()
	{
		 $data = array(
			'Name' => $this->input->post('name5')
		);
		$insert = $this->mm->insert5($data);
		if($insert)
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_5');
		}
		else
		{
			echo $this->session->set_flashdata('msg','"User Added Successfuly"');
			redirect('category_5');

		}

	}
	
	public function delete5($id)
	{
		$deleteUser=$this->mm->delete5($id);
				if($deleteUser)
				{
					echo $this->session->set_flashdata('msg','"User deleted Successfuly"');
					redirect('category_5');
				}
	}

	public function edit5($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit['data']=$this->mm->edit5($id);
		$this->load->view('admin/editUser5',$edit);
		}
		else
		{
			redirect('login');
		}
	}

	public function update5()
	{
		$id=$this->input->post('id');
		$name5=$this->input->post('name5');
		
		$update5=$this->mm->update5($name5,$id);
			if($update5)
			{
				echo $this->session->set_flashdata('msg','"User Updated Successfuly"');
				redirect('category_5');
			}
	}

	// ====================Category -6
	public function category_6()
	{
		if($this->session->userdata('user'))
		{
			$this->load->view('admin/headerlinks');
			$rec['data']=$this->mm->rec6();
			$this->load->view('admin/category_6',$rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function insert6()
	{
		 $data = array(
			'Name' => $this->input->post('name6')
		);
		$insert = $this->mm->insert6($data);
		if($insert)
		{
			echo $this->session->set_flashdata('msg','"Cuisines Added Successfuly"');
			redirect('category_6');
		}

	}
	
	public function delete6($id)
	{
		$deleteUser=$this->mm->delete6($id);
				if($deleteUser)
				{
					echo $this->session->set_flashdata('msg','"Cuisines deleted Successfuly"');
					redirect('category_6');
				}
	}

	public function edit6($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit['data']=$this->mm->edit6($id);
		$this->load->view('admin/editUser6',$edit);
		}
		else
		{
			redirect('login');
		}
	}

	public function update6()
	{
		$id=$this->input->post('id');
		$name6=$this->input->post('name6');
		
		$update6=$this->mm->update6($name6,$id);
			if($update6)
			{
				echo $this->session->set_flashdata('msg','"Cuisines Updated Successfuly"');
				redirect('category_6');
			}
	}


	// ====================Category for Dish Types===========
	public function dish_type()
	{
		if($this->session->userdata('user'))
		{
			$category_4['category_4']=$this->mm->rec4();
			$category_5['category_5']=$this->mm->rec5();
			$this->load->view('admin/headerlinks');
			$rec['data']=$this->mm->dish_type();
			$this->load->view('admin/dish_type',$rec+$category_4+$category_5);
		}
		else
		{
			redirect('login');
		}
	}

	public function dish_type_insert()
	{
		 $data = array(
			'time' => $this->input->post('time'),
			'eating' => $this->input->post('eating'),
			'dish_type' => $this->input->post('dish_type'),
		);
		$insert = $this->mm->dish_type_insert($data);
		if($insert)
		{
			echo $this->session->set_flashdata('msg','"Dish Type Added Successfuly"');
			redirect('dish_type');
		}

	}
	
	public function dish_type_dlt($id)
	{
		$deleteUser=$this->mm->dish_type_dlt($id);
				if($deleteUser)
				{
					echo $this->session->set_flashdata('msg','"Dish Type deleted Successfuly"');
					redirect('dish_type');
				}
	}

// ================USer====================

	//=============chefs Registration===========
	public function chefs()
	{
		if($this->session->userdata('user'))
		{
			$this->load->view('admin/headerlinks');
			$chef_rec['data']=$this->mm->chefs();
			$this->load->view('admin/chefs',$chef_rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function chefRegistration()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$this->load->view('admin/chefRegistration');
		}
		else
		{
			redirect('login');
		}
	}

	public function chef_insertion()
	{
		$chef_data= array(
							'fname' =>	$this->input->post('chef_fname'),
							'lname'	=>	$this->input->post('chef_lname'),
							'bname'	=>	$this->input->post('chef_bname'),
							'email'	=>	$this->input->post('chef_email'),
							'l_num'	=>	$this->input->post('chef_l_num'),
							'mob_num'	=>	$this->input->post('chef_mob_num'),
							'password'	=>	$this->input->post('chef_password'),
							'cpassword'	=>	$this->input->post('chef_c_password'),
						  );
		$chef_insert = $this->mm->chef_insertion($chef_data);
			if($chef_insert)
			{
				echo $this->session->set_flashdata('msg','"Chef Registered Successfuly"');
				redirect('chefs');
			}
	}

	public function dlt_chef($id)
	{
		$dlt_chef=$this->mm->dlt_chef($id);
				if($dlt_chef)
				{
					echo $this->session->set_flashdata('msg','"Chef deleted Successfuly"');
					redirect('chefs');
				}
	}

	public function edit_chefs($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit_chefs['data'] = $this->mm->edit_chefs($id);
		$this->load->view('admin/edit_chefs',$edit_chefs);
		}
		else
		{
			redirect('login');
		}
	}

	public function update_chef()
	{
		$id    = $this->input->post('id');
		$fname = $this->input->post('chef_fname');
		$lname = $this->input->post('chef_lname');
		$bname = $this->input->post('chef_bname');
		$email = $this->input->post('chef_email');
		$l_num = $this->input->post('chef_l_num');
		$mob_num=$this->input->post('chef_mob_num');
		$chef_password=$this->input->post('chef_password');
		$chef_cpassword=$this->input->post('chef_cpassword');

		$update_chef = $this->mm->update_chef($id,$fname,$lname,$bname,$email,$l_num,$mob_num,$chef_password,$chef_cpassword);
			if($update_chef)
			{
				echo $this->session->set_flashdata('msg','"Chef Updated Successfuly"');
				redirect('chefs');
			}
	}

	public function approve_chef()
	{
		$id= $this->input->post('id');
		$approve_chef=$this->mm->approve_chef($id);
		if($approve_chef)
		{
			echo $this->session->set_flashdata('msg','"Chef has Successfuly approved for Login"');
			redirect('chefs');
		}
	}

	public function unapprove_chef()
	{
		$id= $this->input->post('id');
		$unapprove_chef=$this->mm->unapprove_chef($id);
		if($unapprove_chef)
		{
			echo $this->session->set_flashdata('msg','"Chef has Unapproved for Login"');
			redirect('chefs');
		}
	}


	//=============Customers Registration===========
	public function customers()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$csm_rec['data']=$this->mm->customers();
		$this->load->view('admin/customers',$csm_rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function customerRegistration()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$this->load->view('admin/customerRegistration');
		}
		else
		{
			redirect('login');
		}
	}

	public function cus_insertion()
	{
		$cus_password = $this->input->post('cus_password');
		$cus_c_password = $this->input->post('cus_c_password');
		$cus_data= array(
							'fname' =>	$this->input->post('cus_fname'),
							'lname'	=>	$this->input->post('cus_lname'),
							'email'	=>	$this->input->post('cus_email'),
							'mob_num'	=>	$this->input->post('cus_mob_num'),
							'password'	=>	$this->input->post('cus_password'),
							'cpassword'	=>	$this->input->post('cus_c_password'),
						  );
		// if($cus_password != $cus_c_password)
		// {
		// 	echo $this->session->set_flashdata('msg','"Password is not matching, Please type same password in both fields"');
		// 	redirect('customerRegistration');
		// }
		// else
		// {
			$cus_insert = $this->mm->cus_insertion($cus_data);
			if($cus_insert)
			{
				echo $this->session->set_flashdata('msg','"Customer Registered Successfuly"');
				redirect('customers');
			}
			//}
	}

	public function dlt_cus($id)
	{
		$dlt_cus=$this->mm->dlt_cus($id);
				if($dlt_cus)
				{
					echo $this->session->set_flashdata('msg','"Customer deleted Successfuly"');
					redirect('customers');
				}
	}

	public function edit_cus($id)
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$edit_cus['data'] = $this->mm->edit_cus($id);
		$this->load->view('admin/edit_customer',$edit_cus);
		}
		else
		{
			redirect('login');
		}
	}

	public function update_cus()
	{
		$id    = $this->input->post('id');
		$fname = $this->input->post('cus_fname');
		$lname = $this->input->post('cus_lname');
		$email = $this->input->post('cus_email');
		$mob_num=$this->input->post('cus_mob_num');
		$cus_password= $this->input->post('cus_password');
		$cus_c_password= $this->input->post('cus_c_password');

		$update_cus = $this->mm->update_cus($id,$fname,$lname,$email,$mob_num);
			if($update_cus)
			{
				echo $this->session->set_flashdata('msg','"Customer Updated Successfuly"');
				redirect('customers');
			}
	}

	public function approve_cus()
	{
		$id= $this->input->post('id');

		$approve_cus=$this->mm->approve_cus($id);
		if($approve_cus)
		{
			echo $this->session->set_flashdata('msg','"Customer has Successfuly approved for Login"');
			redirect('customers');
		}
	}

	public function unapprove_cus()
	{
		$id= $this->input->post('id');

		$unapprove_cus=$this->mm->unapprove_cus($id);
		if($unapprove_cus)
		{
			echo $this->session->set_flashdata('msg','"Customer has Unapproved for Login"');
			redirect('customers');
		}
	}


// ========Chef Levels==========
	public function levels()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$levels['data']= $this->mm->levels();
		$this->load->view('admin/levels',$levels);
		}
		else
		{
			redirect('login');
		}
	}

	public function add_level_page()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$this->load->view('admin/add_level');
		}
		else
		{
			redirect('login');
		}
	}

	public function insert_level()
	{
		$level = array(
						'name' => $this->input->post('level')
		);
		
		
		$add_level=$this->mm->add_level($level);
			if($add_level)
			{
				$this->session->set_flashdata('msg','Level Added Successfuly');
				redirect('add_level');
			}
	}
// =============================

//============Countries==============

	public function countries()
	{
		if($this->session->userdata('user'))
		{
		$this->load->view('admin/headerlinks');
		$countries['data']= $this->mm->countries();
		$this->load->view('admin/countries',$countries);
		}
		else
		{
			redirect('login');
		}
	}

	public function add_country()
	{
		$add_country = array(
						'Country' => $this->input->post('country')
		);
		$add_country=$this->mm->add_country($add_country);
			if($add_country)
			{
				echo $this->session->set_flashdata('msg','Country Added Successfuly...');
				redirect('countries');
			}
	}

	public function dlt_country($id)
	{
		$dlt_country=$this->mm->dlt_country($id);
				if($dlt_country)
				{
					echo $this->session->set_flashdata('msg','"Country deleted Successfuly"');
					redirect('countries');
				}
	}
// =======================================


//  ===========Cities==============
	public function cities()
	{
		if($this->session->userdata('user'))
		{
		$countries['country']= $this->mm->countries();
		$this->load->view('admin/headerlinks');
		$cities['city']= $this->mm->cities();
		$this->load->view('admin/cities',$countries+$cities);
		}
		else
		{
			redirect('login');
		}
	}

	public function add_city()
	{
		$add_city = array(
						'country' => $this->input->post('country'),
						'city' => $this->input->post('city')
		);
		$add_city=$this->mm->add_city($add_city);
			if($add_city)
			{
				echo $this->session->set_flashdata('msg','City Added Successfuly...');
				redirect('cities');
			}
	}

	public function dlt_city($id)
	{
		$dlt_city=$this->mm->dlt_city($id);
				if($dlt_city)
				{
					echo $this->session->set_flashdata('msg','"City deleted Successfuly"');
					redirect('cities');
				}
	}
// ==================================


// =========States=============
	public function states()
	{
		if($this->session->userdata('user'))
		{
		$countries['country']= $this->mm->countries();
		$this->load->view('admin/headerlinks');
		$states['states']= $this->mm->states();
		$this->load->view('admin/states',$countries+$states);
		}
		else
		{
			redirect('login');
		}
	}

	public function add_state()
	{
		$state = array(
						'country' => $this->input->post('country'),
						'state' => $this->input->post('state')
		);
		$add_state=$this->mm->add_state($state);
			if($add_state)
			{
				echo $this->session->set_flashdata('msg','State Added Successfuly...');
				redirect('states');
			}
	}

	public function dlt_state($id)
	{
		$dlt_state=$this->mm->dlt_state($id);
				if($dlt_state)
				{
					echo $this->session->set_flashdata('msg','"State deleted Successfuly"');
					redirect('states');
				}
	}
// ==================================

//=============Ingredients===========
	public function ingredients()
	{
		if($this->session->userdata('user'))
		{
			$this->load->view('admin/headerlinks');
			$rec['data']=$this->mm->ingredients();
			$this->load->view('admin/ingredients',$rec);
		}
		else
		{
			redirect('login');
		}
	}

	public function insert_ingredients()
	{
		$data = array(
			'ingredients' => $this->input->post('ingredients')
		);

		$insert = $this->mm->insert_ingredients($data);
		if($insert)
		{
			echo $this->session->set_flashdata('msg','"Ingredient Added Successfuly"');
			redirect('ingredients');
		}

	}
	
	public function dlt_ingredient($id)
	{
		$dlt_ingredient=$this->mm->dlt_ingredient($id);
				if($dlt_ingredient)
				{
					echo $this->session->set_flashdata('msg','"ingredient deleted Successfuly"');
					redirect('ingredients');
				}
	} 
}
