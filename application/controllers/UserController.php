<?php

class UserController extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('Mymodel', 'mm');
        $this->load->library('session');
        $this->load->helper('url');
    }

    public function index()
    {


        $uCatered['category_1'] = $this->mm->rec1();    // Region

        $uCatered['category_2'] = $this->mm->rec2();    // Relligion
        $uCatered['category_3'] = $this->mm->rec3();    // Meat
        $uCatered['category_4'] = $this->mm->rec4();    // Time
        $uCatered['category_5'] = $this->mm->rec5();    // Eating
        $uCatered['dish_type'] = $this->mm->dish_type();    // Dish_Type

        $uCatered['dishes'] = $this->mm->fetch_dishes();
        /*print_r($uCatered['dishes'][0]->region);
        die();*/

        $uCatered['title'] = 'uCatered - Online Chef';
        // $id = $this->session->userdata('id');
        // $chef_rec['data']=$this->mm->profile_data($id);

        // $session_data['rec'] = array(
        // 									'email' =>	$where['email'],
        // 									'fname'	=>	$login->fname,
        // 									'lname'	=>	$login->lname,
        // 									'mob_num'	=>$login->mob_num,
        // 									'password'	=>$login->password,
        // 									'cpassword'	=>$login->cpassword
        // 								  );
        // var_dump($session_data);
        // exit();
        // $id=$this->session->set_userdata('cus',$session_data);

        // $data['rec'] = $this->mm->customers_reg_data($id);

        $this->load->view('index', $uCatered);
    }

//==============Customers======================
    public function checkbox()
    {

        $this->load->mm->primary();
        return redirect('dish_dashboard');
    }

    public function cus_insertion()
    {
        $from_email = $this->input->post('cus_email');
        $cus_data = array(
            'fname' => $this->input->post('cus_fname'),
            'lname' => $this->input->post('cus_lname'),
            'email' => $this->input->post('cus_email'),
            'mob_num' => $this->input->post('cus_mob_num'),
            'password' => $this->input->post('cus_password'),
            'cpassword' => $this->input->post('cus_c_password'),
        );

        $cus_insert = $this->mm->cus_insertion($cus_data);
        if ($cus_insert) {
            echo '<p style="color:red">You`ve registered and will be able to Login after Admin approval...</p>';

        } else {
            echo("Error in Query...");
        }
    }


//================Chefs=======================
    public function chef_insertion()
    {
        $chef_data = array(
            'fname' => $this->input->post('chef_fname'),
            'lname' => $this->input->post('chef_lname'),
            'bname' => $this->input->post('chef_bname'),
            'email' => $this->input->post('chef_email'),
            'l_num' => $this->input->post('chef_l_num'),
            'mob_num' => $this->input->post('chef_mob_num'),
            'password' => $this->input->post('chef_password'),
            'cpassword' => $this->input->post('chef_c_password'),
        );
        $chef_insert = $this->mm->chef_insertion($chef_data);
        if ($chef_insert) {
            echo '<p style="color:red">You`ve registered and will be able to Login after Admin approval...</p>';
        } else {
            echo("Error in Query...");
        }
    }
// =============================================

// =================Login USers=================
    public function login_users()
    {

        $role = $this->input->post('role');

        if ($role == 'Login as Chef') {
            $where = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );
            $login = $this->mm->login_chef($where);

            if ($login) {

                $session_data = array(
                    'id' => $login->id,
                    'email' => $where['email'],
                    'fname' => $login->fname,
                    'lname' => $login->lname,
                    'bname' => $login->bname,
                    'l_num' => $login->l_num,
                    'mob_num' => $login->mob_num,
                    'password' => $login->password,
                    'cpassword' => $login->cpassword
                );

                // pre($session_data);

                $this->session->set_userdata('id', $session_data);
                $this->session->set_userdata('cus', $session_data);
                $this->session->set_userdata('role', 'chef');
                return redirect('dashboard');
            } else {
                echo $this->session->set_flashdata('msg', '"Chef Name or Password is Wrong"');
                redirect('index');
            }
        } else if ($role == 'Login as Customer') {

            $where = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );
            $login = $this->mm->login_customers($where);

            if ($login) {
                $session_data['rec'] = array(
                    'id' => $login->id,
                    'email' => $where['email'],
                    'fname' => $login->fname,
                    'lname' => $login->lname,
                    'mob_num' => $login->mob_num,
                    'password' => $login->password,
                    'cpassword' => $login->cpassword
                );
                // pre($session_data);

                $this->session->set_userdata('cus', $session_data);
                $this->session->set_userdata('id', $session_data);
                $this->session->set_userdata('role', 'customer');

                return redirect('user_dashboard');
            } else {
                echo $this->session->set_flashdata('msg', '"Customer Name or Password is Wrong"');
                redirect('index');
            }
        }

    }

    public function jump_to_chef()
    {

        //$this->session->set_userdata('id',$session_data);
        return redirect('dashboard');


    }

    public function jump_to_customer()
    {
        //return redirect('dashboard');
        return redirect('user_dashboard');
    }


    public function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('cus');
        redirect('index');
    }

//==================End User Login============================


// ============Admin Dashboard=====================
    public function dish_dashboard()
    {

        $user = $this->session->userdata('id');


        if ($this->session->userdata('id')) {
            $category_1['category_1'] = $this->mm->rec1();    // Region
            $category_2['category_2'] = $this->mm->rec2();    // Relligion
            $category_3['category_3'] = $this->mm->rec3();    // Meat
            $category_4['category_4'] = $this->mm->rec4();    // Time
            $category_5['category_5'] = $this->mm->rec5();    // Eating
            $category_5['category_6'] = $this->mm->rec6();    // Eating
            $dish_type['dish_type'] = $this->mm->dish_type();    // Dish_Type


            $role = $this->session->userdata('role');
            $dishes['dishes'] = [];
            $user_id = isset($user['id']) ? $user['id'] : 0;

            if ($user_id) {
                switch ($role) {
                    case  ROLE_CHEF:
                        $dishes['dishes'] = $this->mm->fetchChefDishes($user['id']);
                        break;
                    case  ROLE_CUSTOMER:
                        $dishes['dishes'] = $this->mm->fetchCustomerDishes($user['id']);
                        break;
                }
            }
            // $ingredients['ingredients'] = $this->mm->ingredients_suggestion();

            // print_r($dishes);
            // exit();
            // echo json_encode($dishes);
            // print_r($dishes);
            // exit();

            //$request=$this->input->post('request');
            //$search['search']=$this->mm->search($request);

            // $id = $this->session->userdata('id');
            // $chef_rec['name']=$this->mm->profile_data($id);
            /*
        echo "<pre>";
print_r($ingredients['ingredients']);
die();*/

            $this->load->view('dish_dashboard', $category_1 + $category_2 + $category_3 + $category_4 + $category_5 + $dish_type + $dishes);

        } else {
            redirect('index');
        }
    }


    public function receiveJavascriptVar()
    {


        $this->load->mm->checkbox();

        // push to model
    }

    public function fetch()
    {
        $this->load->model('Mymodel');
        echo $this->Mymodel->fetch_data($this->uri->segment(3));
    }


    public function show_dishes()
    {
        $dishes['dishes'] = $this->mm->fetch_dishes();
        // die();
        // pre($dishes);

        // print_r($dishes);
        // exit();
        // echo "sads";
        // exit();
        echo json_encode($dishes);

    }
// ==============================================


// =====================Customers Dashboard==========
    public function dish_dashboard2()
    {
        if ($this->session->userdata('cus')) {
            $category_1['category_1'] = $this->mm->rec1();    // Region
            $category_2['category_2'] = $this->mm->rec2();    // Relligion
            $category_3['category_3'] = $this->mm->rec3();    // Meat
            $category_4['category_4'] = $this->mm->rec4();    // Time
            $category_5['category_5'] = $this->mm->rec5();    // Eating
            $dish_type['dish_type'] = $this->mm->dish_type();    // Dish_Type

            $dishes['dishes'] = $this->mm->fetch_dishes();

            //$request=$this->input->post('request');
            //$search['search']=$this->mm->search($request);
            //$id = $this->session->userdata('id');
            //$chef_rec['data']=$this->mm->profile_data($id);
            $this->load->view('dish_dashboard2', $category_1 + $category_2 + $category_3 + $category_4 + $category_5 + $dish_type + $dishes);
        } else {
            redirect('index');
        }
    }

// ==============================================

    public function add_dish()
    {
        $user_id = $this->session->userdata('id');
        $role = $this->session->userdata('role');

        $data = array(
            'user_id' => $user_id['id'],
            'region' => $this->input->post('region'),
            'religious' => $this->input->post('religious'),
            'meat' => $this->input->post('meat'),
            'time' => $this->input->post('time'),
            'eating' => $this->input->post('eating'),
            'dish_type' => $this->input->post('dish_type'),
            'dish_title' => $this->input->post('dish_title'),
            'regular_price' => $this->input->post('regular_price'),
            'sale_price' => $this->input->post('sale_price'),
            'calories' => $this->input->post('calories'),
            'name' => $this->input->post('name'),
        );

        $config = array();
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 90000;
        $config['max_width'] = 2024;
        $config['max_height'] = 1768;

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload('userfile')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors());

        } else {
            $uploaded = $this->upload->data();

            $data['dish_image'] = str_replace('./', "", $config['upload_path'] . $uploaded['file_name']);

            $add_dish = $this->mm->addDish($data, $role);

            if ($add_dish) {
                $this->session->set_flashdata('msg', 'New Dish Added Successfuly');
            } else {
                echo("Error in Query...");
            }
        }

        redirect('dashboard');

    }

    public function dlt_dish($id)
    {
        $deletedish = $this->mm->dlt_dish($id);
        if ($deletedish) {
            echo $this->session->set_flashdata('msg', '"Dish deleted Successfuly"');
            redirect('dashboard');
        }
    }

    public function ajaxPro()
    {
        $query = $this->input->get('query');
        $this->db->like('ingredients', $query);


        $data = $this->db->get("ingredients")->result();


        echo json_encode($data);
        //return redirect('dish_dashboard');
    }
// =======================================================


//===========Account Settings===========

    public function profile_data()
    {
        if ($this->session->userdata('id')) {
            $id = $this->session->userdata('id');
            // $chef_rec['name']=$this->mm->profile_data($id);
            //$chef_rec['data']=$this->mm->profile_data($id);
            $role = $this->session->userdata('role');
            /*print_r($role);
            die();*/
            if (isset($id['rec'])) {
                $id = $id['rec'];
            }
            if ($role == 'chef') {

                $chef_rec['data'] = $this->mm->profile_data($id);
                /*print_r($id);
                die();*/
            } else {
                $chef_rec['data'] = $this->mm->profile_data2($id);
            }
            $this->load->view('account_settings', $chef_rec);

        } else {
            redirect('index');
        }

    }

    public function update_chef_account()
    {
        $id = $this->input->post('id');
        $fname = $this->input->post('chef_fname');
        $lname = $this->input->post('chef_lname');
        $bname = $this->input->post('chef_bname');
        $email = $this->input->post('chef_email');
        $l_num = $this->input->post('chef_l_num');
        $mob_num = $this->input->post('chef_mob_num');
        $password = $this->input->post('chef_password');
        $cpassword = $this->input->post('chef_c_password');
        $cnic = $this->input->post('chef_cnic');

        $image = $_FILES['image']['name'];
        $path = "userassets/images/cnicf/" . $image;

        move_uploaded_file($_FILES['image']['tmp_name'], $path);

        $image2 = $_FILES['image2']['name'];
        $path2 = "userassets/images/cnicb/" . $image2;

        move_uploaded_file($_FILES['image2']['tmp_name'], $path2);


        $update_chef = $this->mm->update_chef_account($id, $fname, $lname, $bname, $email, $l_num, $mob_num, $password, $cpassword, $cnic, $image, $image2);
        if ($update_chef) {
            echo $this->session->keep_flashdata('msg', '"Chef Updated Successfuly"');
            redirect('profile_data');
        }
    }


    public function profile_data2()
    {


        if ($this->session->userdata('cus')) {
            $id = $this->session->userdata('cus');
            //print_r($id['id']);
            //$id = $id['id'];
            $role = $this->session->userdata('role');
            /*print_r($role);
            die();*/

            if (isset($id['rec'])) {
                $id = $id['rec'];
            }
            if ($role == 'chef') {

                $cus_rec['data'] = $this->mm->profile_data($id);
                /*print_r($id);
                die();*/
            } else {
                $cus_rec['data'] = $this->mm->profile_data2($id);
            }
            /*print_r($cus_rec);
            die();*/
            $this->load->view('account_settings2', $cus_rec);
        } else {
            redirect('index');
        }

    }

    public function update_cus_account()
    {
        $id = $this->input->post('id');
        $fname = $this->input->post('cus_fname');
        $lname = $this->input->post('cus_lname');
        //$bname = $this->input->post('cus_bname');
        $email = $this->input->post('cus_email');
        //$l_num = $this->input->post('cus_l_num');
        $mob_num = $this->input->post('cus_mob_num');
        $password = $this->input->post('cus_password');
        $cpassword = $this->input->post('cus_c_password');
        $cnic = $this->input->post('cus_cnic');

        $image = $_FILES['image']['name'];
        $path = "userassets/images/cnicf/" . $image;

        move_uploaded_file($_FILES['image']['tmp_name'], $path);

        $image2 = $_FILES['image2']['name'];
        $path2 = "userassets/images/cnicb/" . $image2;

        move_uploaded_file($_FILES['image2']['tmp_name'], $path2);


        $update_cus = $this->mm->update_cus_account($id, $fname, $lname, $email, $mob_num, $password, $cpassword, $cnic, $image, $image2);
        if ($update_cus) {
            echo $this->session->keep_flashdata('msg', '"cus Updated Successfuly"');
            redirect('profile_data');
        }
    }

// ===========================================================


// ================Banking section========================
    public function banking_dashboard()
    {
        if ($this->session->userdata('id')) {
            // $id = $this->session->userdata('id');
            // $name['name']=$this->mm->profile_data($id);

            $countries['countries'] = $this->mm->countries();
            $states['states'] = $this->mm->states();
            $cities['cities'] = $this->mm->cities();
            $bank['bank'] = $this->mm->bank();
            $this->load->view('banking_dashboard', $countries + $states + $cities + $bank);
        } else {
            redirect('index');
        }
    }

    public function add_bank_details()
    {
        $bank = array(
            'country' => $this->input->post('country'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'bank_name' => $this->input->post('bank_name'),
            'account_title' => $this->input->post('account_title'),
            'account_number' => $this->input->post('account_number'),
            'iban_number' => $this->input->post('iban_number'),
            'branch_code' => $this->input->post('branch_code'),
            'branch_name' => $this->input->post('branch_name')
        );

        $add_bank_details = $this->mm->add_bank_details($bank);
        if ($add_bank_details) {
            echo $this->session->set_flashdata('msg', 'Details Added Successfuly...');
            redirect('banking_dashboard');
        }
    }

    public function dlt_bank($id)
    {
        $deletebank = $this->mm->dlt_bank($id);
        if ($deletebank) {
            echo $this->session->set_flashdata('msg', '"Bank details deleted Successfuly"');
            redirect('banking_dashboard');
        }
    }
// =====================================================


// ================Coupens=================
    public function coupons()
    {
        if ($this->session->userdata('id')) {
            // $id = $this->session->userdata('id');
            // $name['name']=$this->mm->profile_data($id);
            $coupons['coupons'] = $this->mm->coupons();
            $dish_title['dish_title'] = $this->mm->fetch_dishes();
            $this->load->view('coupons_dashboard', $coupons + $dish_title);
        } else {
            redirect('index');
        }
    }

    public function add_coupon()
    {
        $data = array(
            'coupon_title' => $this->input->post('coupon_title'),
            'coupon_code' => $this->input->post('coupon_code'),
            'activation_date' => $this->input->post('activation_date'),
            'expiry_date' => $this->input->post('expiry_date'),
            'discount' => $this->input->post('discount'),
            'coupon_type' => $this->input->post('coupon_type'),
            'dish_title' => $this->input->post('dish_title')
        );

        $add_coupon = $this->mm->add_coupon($data);
        if ($add_coupon) {
            echo $this->session->set_flashdata('msg', '"Coupon Added Successfuly"');
            redirect('coupons');
        }
    }

    public function dlt_coupon($id)
    {
        $deletecoupon = $this->mm->dlt_coupon($id);
        if ($deletecoupon) {
            echo $this->session->set_flashdata('msg', '"Coupon details deleted Successfuly"');
            redirect('coupons');
        }
    }
// ========================================


// ============Stages======================
    public function stages()
    {
        // $id = $this->session->userdata('id');
        // $name['name']=$this->mm->profile_data($id);
        $stages['stages'] = $this->mm->stages();
        $this->load->view('stages_dashboard', $stages);
    }

// ========================================


    public function getCourse()
    {

        $meal_id = isset($_GET['id'])? $_GET['id'] : 0;

        $json = array();

        $courses = $this->mm->getCourses($meal_id);

        if (count($courses)) {
            $json['success'] = true;
            $json['data'] = $courses;

        }

        if (!count($json)) {
            $json['success'] = false;
            $json['error'] = "Not found";
        }


        echo json_encode($json);
    }

    public function getFoods()
    {

        $course_id = isset($_GET['course_id'])? $_GET['course_id'] : 0;
        $meal_time = isset($_GET['meal_time'])? $_GET['meal_time'] : 0;

        $json = array();

        $foods = $this->mm->getFoods($course_id,$meal_time);

        if (count($foods)) {
            $json['success'] = true;
            $json['data'] = $foods;

        }

        if (!count($json)) {
            $json['success'] = false;
            $json['error'] = "Not found";
        }


        echo json_encode($json);
    }
}

?>