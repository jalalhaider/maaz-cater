<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//===========For Admin==============
//============Login And Food Types=========
$route['default_controller'] = 'UserController';
$route['login']= 'welcome/admin';
$route['change_pass'] = 'welcome/change_pass';
$route['home'] = 'welcome/home';
$route['category_1'] = 'welcome/category_1';
$route['category_2'] = 'welcome/category_2';
$route['category_3'] = 'welcome/category_3';
$route['category_4'] = 'welcome/category_4';
$route['category_5'] = 'welcome/category_5';
$route['category_6'] = 'welcome/category_6';
$route['dish_type']  = 'welcome/dish_type';
$route['levels'] = 'welcome/levels';
$route['add_level'] = 'welcome/add_level_page';
$route['countries'] = 'welcome/countries';
$route['cities'] = 	'welcome/cities';
$route['states'] = 	'welcome/states';
$route['ingredients'] = 'welcome/ingredients';
$route['jump_to_customer'] = 'UserController/jump_to_customer';
$route['jump_to_chef'] = 'UserController/dish_dashboard';
$route['myFunction'] = 'UserController/checkbox';
$route['index']	= 'UserController/homepage';
$route['ajaxpro'] = 'UserController/ajaxPro';
$route['myCheck'] = 'UserController/checkbox';




//===================================


// ==========Chiefs==================
$route['chefs']	= 'welcome/chefs';
//===================================


//===========Customers===============
$route['customerRegistration'] = 'welcome/customerRegistration';
$route['customers']	=	'welcome/customers';
//===================================


//===========For Users==============
$route['index'] = 'UserController/index';
$route['dashboard']	= 'UserController/dish_dashboard';
$route['user_dashboard']	= 'UserController/dish_dashboard2';
$route['profile_data'] = 'UserController/profile_data';
$route['profile_data2'] = 'UserController/profile_data2';
$route['banking_dashboard'] = 'UserController/banking_dashboard';
$route['coupons'] = 'UserController/coupons';
$route['stages'] = 'UserController/stages';
$route['getCourse'] = 'UserController/getCourse';
$route['getFoods'] = 'UserController/getFoods';
//==================================

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
