<?php
class Autocomplete_model extends CI_Model
{
   function fetch_data($query)
   {
    $this->db->like('ingredients', $query);
    $query = $this->db->get('ingredients');
    if($query->num_rows() > 0)
    {
     foreach($query->result_array() as $row)
     {
      $output[] = array(
       'name'  => $row["ingredients"]
      );
     }
     echo json_encode($output);
    }
   }
}

?>