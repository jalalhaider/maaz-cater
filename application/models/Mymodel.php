<?php

/*
Tables
#`category_1` =	cuisines
#`category_2` =	meat_type
#`category_3` =	food_type
#`category_4` =	meal_time
#`category_5` =	food_course
#`category_6` =	food


 * */

class Mymodel extends CI_Model
{
    public function login($email, $pass)
    {
        $query = $this->db->query('select * from data where Email = "' . $email . '" and Password = "' . $pass . '"');
        // if($query->num_rows())
        // {
        // 	return $query->row()->id;
        // }
        // else
        // {
        // 	return false;
        // }
        return $query->result();
    }

    public function update_pass($session_id, $password)
    {
        $query = $this->db->query("update data set Password= '" . $password . "' where id='" . $session_id . "' ");
        return $query;
    }

// ============================================
    public function rec1()
    {
        $query = $this->db->select('*')
            ->from('category_1')
            ->get();
        return $query->result();
    }

    public function insert1($data1)
    {
        return $this->db->insert('category_1', $data1);
    }

    public function delete1($id)
    {
        $query1 = $this->db->query('delete from category_1 where id = "' . $id . '" ');
        return $query1;
    }

    public function edit1($id)
    {
        $query1 = $this->db->select('*')
            ->from('category_1')
            ->where('id', $id)
            ->get();
        return $query1->row();
    }

    public function update1($name, $id) // Updation Query for RegisteredUsers...
    {
        $query1 = $this->db->query("update category_1 set Name= '" . $name . "' where id= '" . $id . "' ");
        return $query1;
    }


    // ======================Category -2
    public function rec2()
    {
        $query = $this->db->select('*')
            ->from('category_2')
            ->get();
        return $query->result();
    }

    public function insert2($data)
    {
        return $this->db->insert('category_2', $data);
    }

    public function delete2($id)
    {
        $query = $this->db->query('delete from category_2 where id = "' . $id . '" ');
        return $query;
    }

    public function edit2($id)
    {
        $query = $this->db->select('*')
            ->from('category_2')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function update2($name, $id) // Updation Query for RegisteredUsers...
    {
        $query = $this->db->query("update category_2 set Name= '" . $name . "' where id= '" . $id . "' ");
        return $query;
    }


    // ======================Category -3
    public function rec3()
    {
        $query = $this->db->select('*')
            ->from('category_3')
            ->get();
        return $query->result();
    }

    public function insert3($data)
    {
        return $this->db->insert('category_3', $data);
    }

    public function delete3($id)
    {
        $query = $this->db->query('delete from category_3 where id = "' . $id . '" ');
        return $query;
    }

    public function edit3($id)
    {
        $query = $this->db->select('*')
            ->from('category_3')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function update3($name, $id) // Updation Query for RegisteredUsers...
    {
        $query = $this->db->query("update category_3 set Name= '" . $name . "' where id= '" . $id . "' ");
        return $query;
    }


    // ======================Category -4

    public function getCourses($meal_id)
    {
        $meal_id = (int)$meal_id;
        $query = $this->db->query("SELECT * FROM `category_5` co INNER JOIN `course_meal` cm ON co.id = cm.`course_id` WHERE cm.`meal_time_id` = $meal_id GROUP BY course_id ");

        return $query->result_array();
    }

    public function getFoods($course_id,$meal_id)
    {
        $course_id = (int)$course_id;
        //$query = $this->db->query("SELECT * FROM `category_6` co WHERE co.`course_id` = $course_id ");
        //$query = $this->db->query("SELECT * FROM `category_6` co INNER JOIN `course_meal` cm ON co.id = cm.`course_id`  WHERE co.`course_id` = $course_id AND cm.`meal_time_id` = $meal_id");
        $query = $this->db->query("SELECT * FROM `course_meal` cm INNER JOIN `category_6` c ON c.id = cm.`food_id` WHERE cm.`course_id` = $course_id AND cm.`meal_time_id` = $meal_id");

        return $query->result_array();
    }

    public function rec4()
    {
        $query = $this->db->select('*')
            ->from('category_4')
            ->get();
        return $query->result();
    }

    public function insert4($data)
    {
        return $this->db->insert('category_4', $data);
    }

    public function delete4($id)
    {
        $query = $this->db->query('delete from category_4 where id = "' . $id . '" ');
        return $query;
    }

    public function edit4($id)
    {
        $query = $this->db->select('*')
            ->from('category_4')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function update4($name, $id) // Updation Query for RegisteredUsers...
    {
        $query = $this->db->query("update category_4 set Name= '" . $name . "' where id= '" . $id . "' ");
        return $query;
    }


    // ======================Category -5
    public function rec5()
    {
        $query = $this->db->select('*')
            ->from('category_5')
            ->get();
        return $query->result();
    }

    public function insert5($data)
    {
        return $this->db->insert('category_5', $data);
    }

    public function delete5($id)
    {
        $query = $this->db->query('delete from category_5 where id = "' . $id . '" ');
        return $query;
    }

    public function edit5($id)
    {
        $query = $this->db->select('*')
            ->from('category_5')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function update5($name5, $id) // Updation Query for RegisteredUsers...
    {
        $query = $this->db->query("update category_5 set Name= '" . $name5 . "' where id= '" . $id . "' ");
        return $query;
    }


    // ======================Category -6
    public function rec6()
    {
        $query = $this->db->select('*')
            ->from('category_6')
            ->get();
        return $query->result();
    }

    public function insert6($data)
    {
        return $this->db->insert('category_6', $data);
    }

    public function delete6($id)
    {
        $query = $this->db->query('delete from category_6 where id = "' . $id . '" ');
        return $query;
    }

    public function edit6($id)
    {
        $query = $this->db->select('*')
            ->from('category_6')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function update6($name6, $id) // Updation Query for RegisteredUsers...
    {
        $query = $this->db->query("update category_6 set Name= '" . $name6 . "' where id= '" . $id . "' ");
        return $query;
    }


    // ======================Category for Dish Types===========
    public function dish_type()
    {
        $query = $this->db->select('*')
            ->from('dish_type')
            ->get();
        return $query->result();
    }

    public function dish_type_insert($data)
    {
        return $this->db->insert('dish_type', $data);
    }

    public function dish_type_dlt($id)
    {
        $query = $this->db->query('delete from dish_type where id = "' . $id . '" ');
        return $query;
    }

    // public function edit6($id)
    // {
    // 	$query = $this->db->select('*')
    // 					  ->from('category_6')
    // 					  ->where('id',$id)
    // 					  ->get();
    // 	return $query->row();
    // }

    // public function update6($name6,$id) // Updation Query for RegisteredUsers...
    // {
    // 	$query = $this->db->query("update category_6 set Name= '".$name6."' where id= '".$id."' ");
    // 	return $query;
    // }

    //================================
    //=================================


    //==============USers================
    //===========Chefs=============
    public function chefs()
    {
        $query = $this->db->select('*')
            ->from('chef')
            ->get();
        return $query->result();
    }

    public function chef_insertion($chef_data)
    {
        return $this->db->insert('chef', $chef_data);
    }

    public function dlt_chef($id)
    {
        $query = $this->db->query('delete from chef where id = "' . $id . '" ');
        return $query;
    }

    public function edit_chefs($id)
    {
        $query = $this->db->select('*')
            ->from('chef')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function update_chef($id, $fname, $lname, $bname, $email, $l_num, $mob_num, $chef_password, $chef_cpassword)
    {
        $query = $this->db->query("update chef set fname= '" . $fname . "' , lname= '" . $lname . "' , bname= '" . $bname . "' , email= '" . $email . "' , l_num= '" . $l_num . "' , mob_num= '" . $mob_num . "'  where id= '" . $id . "' ");
        return $query;
    }

    public function approve_chef($id)
    {
        $query = $this->db->query('update chef set status = 1 where id = "' . $id . '" ');
        return $query;
    }

    public function unapprove_chef($id)
    {
        $query = $this->db->query('update chef set status = 0 where id = "' . $id . '" ');
        return $query;
    }
//=============================================


    //============Customers===============
    public function customers()
    {
        $query = $this->db->select('*')
            ->from('customers')
            ->get();
        return $query->result();
    }

    public function cus_insertion($cus_data)
    {
        return $this->db->insert('customers', $cus_data);
    }

    public function dlt_cus($id)
    {
        $query = $this->db->query('delete from customers where id = "' . $id . '" ');
        return $query;
    }

    public function edit_cus($id)
    {
        $query = $this->db->select('*')
            ->from('customers')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function update_cus($id, $fname, $lname, $email, $mob_num)
    {
        $query = $this->db->query("update customers set fname= '" . $fname . "' , lname= '" . $lname . "' , email= '" . $email . "' , mob_num= '" . $mob_num . "' where id= '" . $id . "' ");
        return $query;
    }

    public function approve_cus($id)
    {
        $query = $this->db->query('update customers set status = 1 where id = "' . $id . '" ');
        return $query;
    }

    public function unapprove_cus($id)
    {
        $query = $this->db->query('update customers set status = 0 where id = "' . $id . '" ');
        return $query;
    }

    public function login_chef($where)
    {
        $query = $this->db->where($where)
            ->where('status', '1')
            ->get('chef');

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function login_customers($where)
    {
        $query = $this->db->where($where)
            ->where('status', '1')
            ->get('customers');
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }
//================================================


//==================Account Settings==============
    public function profile_data($id)
    {
        /*print_r($id);
        die();*/

        $query = $this->db->where($id)
            ->get('chef');
        // if($query->num_rows()>0)
        // {
        return $query->row();
        //}
    }

    public function update_chef_account($id, $fname, $lname, $bname, $email, $l_num, $mob_num, $password, $cpassword, $cnic, $image, $image2)
    {
        $query = $this->db->query("update chef set fname= '" . $fname . "' , lname= '" . $lname . "' , bname= '" . $bname . "' , email= '" . $email . "' , l_num= '" . $l_num . "' , mob_num= '" . $mob_num . "', password = '" . $password . "', cpassword = '" . $cpassword . "', cnic = '" . $cnic . "', chef_cnic_fpic = '" . $image . "',  chef_cnic_bpic = '" . $image2 . "' where id= '" . $id . "' ");
        return $query;
    }


    public function profile_data2($id)
    {
        /*print_r($id);
        die();*/
        $query = $this->db->where($id)
            ->get('customers');

        // if($query->num_rows()>0)
        // {
        return $query->row();
        //}
    }

    public function update_cus_account($id, $fname, $lname, $bname, $email, $l_num, $mob_num, $password, $cpassword, $cnic, $image, $image2)
    {
        $query = $this->db->query("update customers set fname= '" . $fname . "' , lname= '" . $lname . "' , bname= '" . $bname . "' , email= '" . $email . "' , l_num= '" . $l_num . "' , mob_num= '" . $mob_num . "', password = '" . $password . "', cpassword = '" . $cpassword . "', cnic = '" . $cnic . "', chef_cnic_fpic = '" . $image . "',  chef_cnic_bpic = '" . $image2 . "' where id= '" . $id . "' ");
        return $query;
    }

//================================================


// ========Chef Levels==========
    public function levels()
    {
        $query = $this->db->select('*')
            ->from('stages')
            ->get();
        return $query->result();
    }

    public function add_level($level)
    {
        return $this->db->insert('stages', $level);
    }
//=============================================


//============Dishes==============
    public function primary()
    {
        //$this->db->update('bank')->where(bank)
    }

    public function checkbox()
    {
        $myJSVar = $this->input->post('bank');
    }

    public function add_dish($data)
    {

        $sql = "";


    }

    public function addDish($data, $role)
    {

        $region = $data['region'];
        $name = $data['name'];
        $religious = $data['religious'];
        $meat = $data['meat'];
        $time = $data['time'];
        $eating = $data['eating'];
        $dish_type = $data['dish_type'];
        $dish_title = $data['dish_title'];
        $regular_price = $data['regular_price'];
        $sale_price = $data['sale_price'];
        $calories = $data['calories'];
        $dish_image = $data['dish_image'];
        //$used_ingredients    =             $data['used_ingredients'] || "";
        //$coupon              =             $data['coupon'] || "";
        //$ingredients         =             $data['ingredients'] || "";

        $sql = "INSERT INTO `dishes` SET 
          region              =             '$region',
          name                =             '$name',
          religious           =             '$religious',
          meat                =             '$meat',
          time                =             '$time',
          eating              =             '$eating',
          dish_type           =             '$dish_type',
          dish_title          =             '$dish_title',
          regular_price       =             '$regular_price',
          sale_price          =             '$sale_price',
          dish_image          =             '$dish_image',
          calories            =             '$calories'";

        $result = $this->db->query($sql);

        if ($result) {
            $dish_id = $this->db->insert_id();
            $user_id = $data['user_id'];

            switch ($role) {
                case  ROLE_CHEF:
                    $result = $this->db->insert('chef_dishes', array(
                        'chef_id' => $user_id,
                        'dish_id' => $dish_id,
                        'is_active' => 1
                    ));
                    break;
                case  ROLE_CUSTOMER:
                    $result = $this->db->insert('customer_dishes', array(
                        'chef_id' => $user_id,
                        'dish_id' => $dish_id,
                        'is_active' => 1
                    ));
                    break;


            }
        }
        return $result;
    }


    public function ingredients_suggestion()
    {
        $query = $this->db->get('ingredients');
        $this->db->like('ingredients', '$query');


        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $output[] = array(
                    'name' => $row["ingredients"],

                );
            }
            /* print_r($output);
             die();*/
            json_encode($output);
            return $query->result();
        }
    }

    public function fetch_dishes($where = array())
    {
        $query = $this->db->where($where)->get('dishes');
        return $query->result();

    }

    public function dlt_dish($id)
    {
        $query1 = $this->db->query('delete from dishes where id = "' . $id . '" ');
        return $query1;
    }

    public function fetch_data($query)
    {
        $this->db->like('ingredients', $query);
        $query = $this->db->get('ingredients');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $output[] = array(
                    'name' => $row["ingredients"]
                );
            }
            echo json_encode($output);
        }
    }

    public function fetchChefDishes($chef_id)
    {

        $query = $this->db->query("SELECT * FROM dishes d INNER JOIN chef_dishes cd ON d.id = cd.dish_id WHERE cd.chef_id='$chef_id'");

        return $query->result();
    }

    public function fetchCustomerDishes($customer_id)
    {

        $query = $this->db->query("SELECT * FROM dishes d INNER JOIN customer_dishes cd ON d.id = cd.dish_id WHERE cd.customer_id='$customer_id'");

        return $query->result();
    }


// ===============================


    public function customers_reg_data($id)
    {
        $query = $this->db->where($id)
            ->get('customers');

        return $query->row();

    }
// ================================


// =============Bank Details========================
    public function bank()
    {
        $query = $this->db->select('*')
            ->from('bank')
            ->get();
        return $query->result();
    }

    public function add_bank_details($bank)
    {
        return $this->db->insert('bank', $bank);
    }

    public function dlt_bank($id)
    {
        $query1 = $this->db->query('delete from bank where id = "' . $id . '" ');
        return $query1;
    }
// =================================================


// ============Countries=================
    public function countries()
    {
        $query = $this->db->select('*')
            ->from('countries')
            ->get();
        return $query->result();
    }

    public function add_country($country)
    {
        return $this->db->insert('countries', $country);
    }

    public function dlt_country($id)
    {
        $query = $this->db->query('delete from countries where id = "' . $id . '" ');
        return $query;
    }
// =============================================

// ==========Cities============
    public function cities()
    {
        $query = $this->db->select('*')
            ->from('cities')
            ->get();
        return $query->result();
    }

    public function add_city($city)
    {
        return $this->db->insert('cities', $city);
    }

    public function dlt_city($id)
    {
        $query = $this->db->query('delete from cities where id = "' . $id . '" ');
        return $query;
    }
// =====================================


// ==========Cities====================
    public function states()
    {
        $query = $this->db->select('*')
            ->from('states')
            ->get();
        return $query->result();
    }

    public function add_state($state)
    {
        return $this->db->insert('states', $state);
    }

    public function dlt_state($id)
    {
        $query = $this->db->query('delete from states where id = "' . $id . '" ');
        return $query;
    }
// =====================================

//============Ingredients==============
    public function insert_ingredients($data)
    {
        return $this->db->insert('ingredients', $data);
    }

    public function ingredients()
    {
        $query = $this->db->get('ingredients');
        return $query->result();

    }

    public function ajaxPro()
    {
        $query = $this->input->get('query');
        $this->db->like('ingredients', $query);


        $data = $this->db->get("ingredients")->result();


        echo json_encode($data);
        //return redirect('dish_dashboard');
    }

    public function dlt_ingredient($id)
    {
        $query1 = $this->db->query('delete from ingredients where id = "' . $id . '" ');
        return $query1;
    }
// ===============================


// ============Coupans============
    public function coupons()
    {
        $query = $this->db->select('*')
            ->from('coupons')
            ->get();
        return $query->result();
    }

    public function add_coupon($data)
    {
        return $this->db->insert('coupons', $data);
        return $this->db->query('update dishes set coupon="applied"');
    }

    public function dlt_coupon($id)
    {
        $query1 = $this->db->query('delete from coupons where id = "' . $id . '" ');
        return $query1;
    }
// ===============================


// ============Stages======================
    public function stages()
    {
        $query = $this->db->select('*')
            ->from('stages')
            ->get();
        return $query->result();
    }
// ========================================
}

?>