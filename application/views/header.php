<!-- Header -->
	<header>
		<!-- Header desktop -->
		<div class="container-menu-header">

			<div class="wrap_header site-navbar">
				<!-- Logo -->
				<a href="<?=base_url('dashboard')?>" class="logo">
						<img src="<?=base_url('userassets/images/logo.png')?>" alt="IMG-LOGO">
				</a>
				<div class="col-12">
              <nav class="site-navigation text-right ml-auto " role="navigation">

                <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                  <li><a href="#home-section" class="nav-link" data-toggle="modal" data-target="#registermodal">Register</a></li>
                  <li><a href="#services-section" class="nav-link" data-toggle="modal" data-target="#loginmodal">Login</a></li>
                  <li class="header-wrapicon2"><a href="#" class="nav-link js-show-header-dropdown">My Account</a>
						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
							<ul>
								<li class="header-cart-item custom-drop">
									<div class="header-cart-item">
										<strong class="header-h1"><i class="fa fa-check"></i> David Parker</strong>	
									</div>								
								</li>
								<li class="header-cart-item custom-drop">
								<a href="#" class="custom-drop-a"><i class="fa fa-tachometer"></i> Dashboad</a>
								</li>
								<li class="header-cart-item custom-drop">
								<a href="<?=base_url('profile_data')?>" class="custom-drop-a"> <i class="fa fa-user"></i> Profile</a>
								</li>
								<li class="header-cart-item custom-drop">
								<a href="#" class="custom-drop-a"><i class="fa fa-phone"></i> Help & Support</a>
								</li>								
								<li class="header-cart-item custom-drop" style="border-bottom: 0;">
								<a href="#" class="custom-drop-a"><i class="fa fa-sign-out"></i> Logout</a>
								</li>								

							</ul>
						</div>
                  </li> 

				  <!-- <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
					  aria-haspopup="true" aria-expanded="false">Dropdown</a>
					<div class="dropdown-menu dropdown-primary col-md-12" aria-labelledby="navbarDropdownMenuLink">
						<div class="row">
							<div class="col-sm-12">
								<a class="dropdown-item" href="#"> <img src="images/person_1.jpg" style="width: 100px; height: 100px;" alt=""> </a>
							</div>
						<div class="row">
							<div class="col-xs-6"> <i class="fa fa-user"></i></div>
								<div class="col-xs-6"><a class="dropdown-item" href="#">Profile</a></div>
						</div>
						<div class="row">
							<div class="col-xs-6"><i class="fa fa-settings"></i></div>
							<div class="col-xs-6"><a class="dropdown-item" href="#">Settings</a></div>
						</div>
						<div class="row">
							<div class="col-xs-6"><i class="fa fa-logout"></i></div>
							<div class="col-xs-6"><button class="dropdown-item">Log Out</button></div>
						</div>
							
						</div>
						
					</div>
				  </li> -->
				  
				  
				</ul>
				
              </nav>

            </div>

				<!-- Header Icon -->
				
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="<?=base_url('home')?>" class="logo-mobile">
				<img src="<?=base_url('userassets/images/icons/logo.png')?>" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
					<a href="#" class="header-wrapicon1 dis-block">
						<img src="<?=base_url('userassets/images/icons/icon-header-01.png')?>" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<img src="<?=base_url('userassets/images/icons/icon-header-02.png')?>" class="header-icon1 js-show-header-dropdown" alt="ICON">
						<span class="header-icons-noti">0</span>

						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem">
								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?=base_url('userassets/images/item-cart-01.jpg')?>" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="#" class="header-cart-item-name">
											White Shirt With Pleat Detail Back
										</a>

										<span class="header-cart-item-info">
											1 x $19.00
										</span>
									</div>
								</li>

								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?=base_url('userassets/images/item-cart-02.jpg')?>" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="#" class="header-cart-item-name">
											Converse All Star Hi Black Canvas
										</a>

										<span class="header-cart-item-info">
											1 x $39.00
										</span>
									</div>
								</li>

								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?=base_url('userassets/images/item-cart-03.jpg')?>" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="#" class="header-cart-item-name">
											Nixon Porter Leather Watch In Tan
										</a>

										<span class="header-cart-item-info">
											1 x $17.00
										</span>
									</div>
								</li>
							</ul>

							<div class="header-cart-total">
								Total: $75.00
							</div>

							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										View Cart
									</a>
								</div>

								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Check Out
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">
					<!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							Free shipping for standard order over $100
						</span>
					</li> -->

					<!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<div class="topbar-child2-mobile">
							<span class="topbar-email">
								fashe@example.com
							</span>

							<div class="topbar-language rs1-select2">
								<select class="selection-1" name="time">
									<option>USD</option>
									<option>EUR</option>
								</select>
							</div>
						</div>
					</li> -->

					<li class="item-topbar-mobile p-l-10">
						<div class="topbar-social-mobile">
							<a href="#" class="topbar-social-item fa fa-facebook"></a>
							<a href="#" class="topbar-social-item fa fa-instagram"></a>
							<a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
							<a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
							<a href="#" class="topbar-social-item fa fa-youtube-play"></a>
						</div>
					</li>

					


					<li class="item-menu-mobile">
						<a href="about.html">Become a Chef</a>
					</li>

					<li class="item-menu-mobile">
						<a href="contact.html">Register User</a>
					</li>
					<li class="item-menu-mobile">
							<a href="contact.html">0800 1234567</a>
					</li>
				</ul>
			</nav>
		</div>
	</header>