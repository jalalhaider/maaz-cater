<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>uCatered - Admin Portal</title>

  <!-- Favicons -->
  <link href="<?=base_url('adminassets/img/favicon.png')?>" rel="icon">
  <link href="<?=base_url('adminassets/img/apple-touch-icon.png')?>" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="<?=base_url('adminassets/lib/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
  <!--external css-->
  <link href="<?=base_url('adminassets/lib/font-awesome/css/font-awesome.css')?>" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="<?=base_url('adminassets/lib/advanced-datatable/css/demo_page.css')?>">
  <!-- <link rel="stylesheet" type="text/css" href="<?=base_url('adminassets/lib/advanced-datatable/css/demo_table.css')?>" /> -->
  <!-- Custom styles for this template -->
  <link href="<?=base_url('adminassets/css/style.css')?>" rel="stylesheet">
  <link href="<?=base_url('adminassets/css/style-responsive.css')?>" rel="stylesheet">
  <script src="<?base_url('adminassets/lib/chart-master/Chart.js')?>"></script>

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>
<body>
   <!--main content start-->
    <center>
    <section id="main-content">
      <section class="wrapper" style="width: 60%; padding-top: 5%;">
        <h3><i class=""></i><h1>Chef Levels</h1></h3>
        <center><p style="color: red;"><?php echo $this->session->flashdata('msg');?></p><br></center>
        <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
              <table style="width: 60%;" class="display table table-bordered table table-hover table-fixed" id="hidden-table-info">
                <thead>
                  <tr>
                    <th>Sno</th>
                    <th>Name</th>
                  </tr>
                </thead>
                <tbody>
                
                  <?php
                $sno = 1;
                  if (count($data)) 
                            {
                              foreach($data as $value)
                              {
                        ?>
                        <tr style="text-align: center;" class="gradeX">
                            <td><?php echo $sno++?></td>
                            <td><?php echo $value->name;?></td>
                        </tr>
                        <?php
                              }
                            }
              ?>
                  
                </tbody>
              </table>
              <a href="<?=base_url('add_level')?>"><button class="btn btn-primary">Add Level</button></a></center>
            </div>
          </div>
          <!-- page end-->
        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->

     <!--footer start-->
<p style="padding-top: 25%;"></p>
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="advanced_table.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="<?=base_url('adminassets/lib/jquery/jquery.min.js')?>"></script>
  <script type="text/javascript" language="javascript" src="<?=base_url('adminassets/lib/advanced-datatable/js/jquery.js')?>"></script>
  <script src="<?=base_url('adminassets/lib/bootstrap/js/bootstrap.min.js')?>"></script>
  <script class="include" type="text/javascript" src="<?=base_url('adminassets/lib/jquery.dcjqaccordion.2.7.js')?>"></script>
  <script src="<?=base_url('adminassets/lib/jquery.scrollTo.min.js')?>"></script>
  <script src="<?=base_url('adminassets/lib/jquery.nicescroll.js')?>" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="<?=base_url('adminassets/lib/advanced-datatable/js/jquery.dataTables.js')?>"></script>
  <script type="text/javascript" src="<?=base_url('adminassets/lib/advanced-datatable/js/DT_bootstrap.js')?>"></script>
  <!--common script for all pages-->
  <script src="<?=base_url('adminassets/lib/common-scripts.js')?>"></script>
  
</body>
</html>