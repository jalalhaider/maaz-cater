
<section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="<?=base_url('home')?>" class="logo"><b>uCatered</b></a>
      <!--logo end-->
      
        
      
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <?php
            if(!$this->session->userdata('user'))
            {
          ?>
           <li><a class="login" href="<?=base_url('login')?>">login</a></li>
          <?php
            }
          else
          {
          ?>
            
           
            <li><a class="logout" href="<?=base_url('Welcome/logout')?>">logout</a></li>
          <?php
          }
          ?>
         
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <li class="mt">
            <a class="active" href="<?=base_url('home')?>">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>

          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span>Type of Foods</span>
              </a>
            <ul class="sub">
              <li><a href="<?=base_url('category_1')?>">Region</a></li>
              <li><a href="<?=base_url('category_2')?>">Religious</a></li>
              <li><a href="<?=base_url('category_3')?>">Meat</a></li>
              <li><a href="<?=base_url('category_4')?>">Time</a></li>
              <li><a href="<?=base_url('category_5')?>">Eating</a></li>
              <li><a href="<?=base_url('category_6')?>">Cusines</a></li>
              <li><a href="<?=base_url('dish_type')?>">Dish Types</a></li>
            </ul>
          </li>

          <li class="mt">
            <a class="inactive" href="<?=base_url('chefs')?>">
              <i class="fa fa-user"></i>
              <span>Chefs</span>
              </a>
          </li>

          <li class="mt">
            <a class="inactive" href="<?=base_url('customers')?>">
              <i class="fa fa-user"></i>
              <span>Customers</span>
              </a>
          </li>

           <li class="mt">
            <a class="inactive" href="<?=base_url('levels')?>">
              <i class="fa fa-user"></i>
              <span>Levels</span>
              </a>
          </li>

          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-user"></i>
              <span>Location</span>
              </a>
            <ul class="sub">
              <li><a href="<?=base_url('countries')?>">Countries</a></li>
              <li><a href="<?=base_url('cities')?>">Cities</a></li>
              <li><a href="<?=base_url('states')?>">States</a></li>
            </ul>
          </li>

          <li class="mt">
            <a class="inactive" href="<?=base_url('ingredients')?>">
              <i class="fa fa-user"></i>
              <span>Ingredients</span>
              </a>
          </li>

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>