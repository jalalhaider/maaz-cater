<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?=base_url('userassets/images/icons/favicon.png')?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/bootstrap/css/bootstrap.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/themify/themify-icons.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/elegant-font/html-css/style.css')?>">
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">--> 
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/animsition/css/animsition.min.css')?>">
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">-->
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/slick/home-slick.css')?>">
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-util.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-main.css')?>">
<!--===============================================================================================-->
<!-- CARGO_CSS -->
<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-style.css')?>">
<!-- MY_CSS -->
<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-mystyle.css')?>">
<!-- LOGO CAROUSEL JAVA <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script> -->

<script src="<?=base_url('//code.jquery.com/jquery-1.11.1.min.js')?>"></script>
<script src="<?=base_url('https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js')?>"></script>
</head>


<!-- Body Starts Here........... -->
<body>

</body>
</html>