<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    <title>Starter Template for Bootstrap</title>
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url('userassets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('userassets/css/font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('userassets/css/animsition.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('userassets/css/animate.css')?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url('userassets/css/style.css')?>" rel="stylesheet"> 

    <!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?=base_url('userassets/images/icons/favicon.png')?>"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/bootstrap/css/bootstrap.min.css')?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/themify/themify-icons.css')?>">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/elegant-font/html-css/style.css')?>">
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">--> 
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> -->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/animsition/css/animsition.min.css')?>">
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">-->
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css"> -->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/slick/home-slick.css')?>">
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css"> -->
    <!--===============================================================================================-->

<!--===============================================================================================-->
     <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/bootstrap/css/bootstrap.min.css')?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-util.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-main.css')?>">
<!--===============================================================================================-->    
    <!-- CARGO_CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-style.css')?>">
    <!-- MY_CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-mystyle.css')?>">
<!-- LOGO CAROUSEL JAVA -->
<link href="<?=base_url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css')?>" rel="stylesheet" id="bootstrap-css">
<script src="<?=base_url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js')?>"></script>
<script src="<?=base_url('//code.jquery.com/jquery-1.11.1.min.js')?>"></script>
</head>
<body>
    <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <!-- Header -->
    <header>
        <!-- Header desktop -->
        <div class="container-menu-header">
            <!-- <div class="topbar">
                <div class="topbar-social">
                    <a href="#" class="topbar-social-item fa fa-facebook"></a>
                    <a href="#" class="topbar-social-item fa fa-instagram"></a>
                    <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                    <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                    <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                </div>

                <span class="topbar-child1">
                    Free shipping for standard order over $100
                </span>

                <div class="topbar-child2">
                    <span class="topbar-email">
                        fashe@example.com
                    </span>

                    <div class="topbar-language rs1-select2">
                        <select class="selection-1" name="time">
                            <option>USD</option>
                            <option>EUR</option>
                        </select>
                    </div>
                </div>
            </div> -->

            <div class="wrap_header site-navbar">
                <!-- Logo -->
                <a href="index.html" class="logo">
                        <img src="<?=base_url('userassets/images/logo.png')?>" alt="IMG-LOGO">
                </a>
;asLLA'DLA'DS;L'ASLD'
                <!-- Menu -->
                <!-- <div class="wrap_menu ">
                    <nav class="menu">
                        <ul class="main_menu">

                            <li>
                                <a href="about.html" class="shadow p-3 mb-5">Become a Chef</a>
                            </li>

                            <li>
                                <a href="contact.html" class="shadow p-3 mb-5">Register User</a>
                            </li>
                        </ul>
                    </nav>
                </div> -->
                <div class="col-12">
              <nav class="site-navigation text-right ml-auto " role="navigation">

                <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                    <li><a href="#home-section" class="nav-link" data-toggle="modal" data-target="#registermodal">Be a Chef</a></li>    
                  <li><a href="#home-section" class="nav-link" data-toggle="modal" data-target="#registermodal">Register</a></li>
                  <li><a href="#services-section" class="nav-link" data-toggle="modal" data-target="#loginmodal">Login</a></li>


                  <li class="has-children">
                    <a href="tell://08001234567"  class="nav-link">0800 1234567</a>
                  </li>
                  
                </ul>
                
              </nav>

            </div>

                <!-- Header Icon -->
                
            </div>
        </div>

        <!-- Header Mobile -->
        <div class="wrap_header_mobile">
            <!-- Logo moblie -->
            <a href="index.html" class="logo-mobile">
                <img src="userassets/images/icons/logo.png" alt="IMG-LOGO">
            </a>

            <!-- Button show menu -->
            <div class="btn-show-menu">
                <!-- Header Icon mobile -->
                <div class="header-icons-mobile">
                    <a href="#" class="header-wrapicon1 dis-block">
                        <img src="userassets/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                    </a>

                    <span class="linedivide2"></span>

                    <div class="header-wrapicon2">
                        <img src="userassets/images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <span class="header-icons-noti">0</span>

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <ul class="header-cart-wrapitem">
                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        <img src="userassets/images/item-cart-01.jpg" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            White Shirt With Pleat Detail Back
                                        </a>

                                        <span class="header-cart-item-info">
                                            1 x $19.00
                                        </span>
                                    </div>
                                </li>

                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        <img src="userassets/images/item-cart-02.jpg" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            Converse All Star Hi Black Canvas
                                        </a>

                                        <span class="header-cart-item-info">
                                            1 x $39.00
                                        </span>
                                    </div>
                                </li>

                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        <img src="userassets/images/item-cart-03.jpg" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            Nixon Porter Leather Watch In Tan
                                        </a>

                                        <span class="header-cart-item-info">
                                            1 x $17.00
                                        </span>
                                    </div>
                                </li>
                            </ul>

                            <div class="header-cart-total">
                                Total: $75.00
                            </div>

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        View Cart
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                        Check Out
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </div>
            </div>
        </div>

        <!-- Menu Mobile -->
        <div class="wrap-side-menu" >
            <nav class="side-menu">
                <ul class="main-menu">
                    <!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                        <span class="topbar-child1">
                            Free shipping for standard order over $100
                        </span>
                    </li> -->

                    <!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                        <div class="topbar-child2-mobile">
                            <span class="topbar-email">
                                fashe@example.com
                            </span>

                            <div class="topbar-language rs1-select2">
                                <select class="selection-1" name="time">
                                    <option>USD</option>
                                    <option>EUR</option>
                                </select>
                            </div>
                        </div>
                    </li> -->

                    <li class="item-topbar-mobile p-l-10">
                        <div class="topbar-social-mobile">
                            <a href="#" class="topbar-social-item fa fa-facebook"></a>
                            <a href="#" class="topbar-social-item fa fa-instagram"></a>
                            <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                            <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                            <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                        </div>
                    </li>

                    


                    <li class="item-menu-mobile">
                        <a href="about.html">Become a Chef</a>
                    </li>

                    <li class="item-menu-mobile">
                        <a href="contact.html">Register User</a>
                    </li>
                    <li class="item-menu-mobile">
                            <a href="contact.html">0800 1234567</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
        <div class="page-wrapper">
            <!-- top Links -->
            <div class="top-links">
                <div class="container">
                    <ul class="row links">
                        <li class="col-xs-12 col-sm-3 link-item"><span>1</span><a href="index.html">Choose Your Location</a></li>
                        <li class="col-xs-12 col-sm-3 link-item active"><span>2</span><a href="restaurants.html">Choose Chef</a></li>
                        <li class="col-xs-12 col-sm-3 link-item"><span>3</span><a href="profile.html">Pick Your favorite food</a></li>
                        <li class="col-xs-12 col-sm-3 link-item"><span>4</span><a href="checkout.html">Order and Pay online</a></li>
                    </ul>
                </div>
            </div>
            <!-- end:Top links -->
            <!-- start: Inner page hero -->
            <div class="inner-page-hero bg-image" data-image-src="http://placehold.it/1670x480">
                <div class="container"> </div>
                <!-- end:Container -->
            </div>
            <div class="result-show">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <p><span class="primary-color"><strong>124</strong></span> Results so far </div>
                        </p>
                        <div class="col-sm-9">
                            <select class="custom-select pull-right">
                                <option selected>Open this select menu</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //results show -->
            <section class="restaurants-page">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-3">
                            <div class="sidebar clearfix m-b-20">
                                <div class="main-block">
                                    <div class="sidebar-title white-txt">
                                        <h6>Choose Cusine</h6> <i class="fa fa-cutlery pull-right"></i> </div>
                                    <div class="input-group">
                                        <input type="text" class="form-control search-field" placeholder="Search your favorite food"> <span class="input-group-btn"> 
                                 <button class="btn btn-secondary search-btn" type="button"><i class="fa fa-search"></i></button> 
                                 </span> </div>
                                    <form>
                                        <ul>
                                            <li>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Barbecuing and Grilling</span> </label>
                                            </li>
                                            <li>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Appetizers</span> </label>
                                            </li>
                                            <li>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Soup and salads</span> </label>
                                            </li>
                                            <li>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Seafood</span> </label>
                                            </li>
                                            <li>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Beverages</span> </label>
                                            </li>
                                        </ul>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                                <!-- end:Sidebar nav -->
                                <div class="widget-delivery">
                                    <form>
                                        <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
                                            <label class="custom-control custom-radio">
                                                <input id="radio1" name="radio" type="radio" class="custom-control-input" checked=""> <span class="custom-control-indicator"></span> <span class="custom-control-description">Delivery</span> </label>
                                        </div>
                                        <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
                                            <label class="custom-control custom-radio">
                                                <input id="radio2" name="radio" type="radio" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Takeout</span> </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="widget clearfix">
                                <!-- /widget heading -->
                                <div class="widget-heading">
                                    <h3 class="widget-title text-dark">
                                 Price range
                              </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="widget-body">
                                    <div class="range-slider m-b-10"> <span id="ex2CurrentSliderValLabel"> Filter by price:<span id="ex2SliderVal"><strong>35</strong></span>€</span>
                                        <br>
                                        <input id="ex2" type="text" data-slider-min="1" data-slider-max="100" data-slider-step="1" data-slider-value="35" /> </div>
                                </div>
                            </div>
                            <!-- end:Pricing widget -->
                            <div class="widget clearfix">
                                <!-- /widget heading -->
                                <div class="widget-heading">
                                    <h3 class="widget-title text-dark">
                                 Popular tags
                              </h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="widget-body">
                                    <ul class="tags">
                                        <li> <a href="#" class="tag">
                                    Pizza
                                    </a> </li>
                                        <li> <a href="#" class="tag">
                                    Sendwich
                                    </a> </li>
                                        <li> <a href="#" class="tag">
                                    Sendwich
                                    </a> </li>
                                        <li> <a href="#" class="tag">
                                    Fish 
                                    </a> </li>
                                        <li> <a href="#" class="tag">
                                    Desert
                                    </a> </li>
                                        <li> <a href="#" class="tag">
                                    Salad
                                    </a> </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end:Widget -->
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-9">
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                            <div class="bg-gray restaurant-entry">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
                                        <div class="entry-logo">
                                            <a class="img-fluid" href="#"><img src="http://placehold.it/110x110" alt="Food logo"></a>
                                        </div>
                                        <!-- end:Logo -->
                                        <div class="entry-dscr">
                                            <h5><a href="#">Maenaam Thai Chef</a></h5> <span>Burgers, American, Sandwiches, Fast Food, BBQ,urgers, American, Sandwiches <a href="#">...</a></span>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-check"></i> Min $ 10,00</li>
                                                <li class="list-inline-item"><i class="fa fa-motorcycle"></i> 30 min</li>
                                            </ul>
                                        </div>
                                        <!-- end:Entry description -->
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
                                        <div class="right-content bg-white">
                                            <div class="right-review">
                                                <div class="rating-block"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                                <p> 245 Reviews</p> <a href="profile.html" class="btn theme-btn-dash">View Menu</a> </div>
                                        </div>
                                        <!-- end:right info -->
                                    </div>
                                </div>
                                <!--end:row -->
                            </div>
                            <!-- end:Restaurant entry -->
                        </div>
                    </div>
                </div>
            </section>

            <footer class=" p-t-45 p-b-43 p-l-45 p-r-45" style="background-image: url('userassets/images/footer_img5.jpg'); background-size: cover">
                <div class="flex-w p-b-90">
                    <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
                        <h4 class="s-text12 p-b-30">
                            <img src="images/logo.png" class="footer-logo" alt="IMG-LOGO">
                        </h4>
                        <div>
                                <a href="#" class="fs-18 color0 p-r-20 fa fa-facebook"></a>
                                <a href="#" class="fs-18 color0 p-r-20 fa fa-instagram"></a>
                                <a href="#" class="fs-18 color0 p-r-20 fa fa-pinterest-p"></a>
                                <a href="#" class="fs-18 color0 p-r-20 fa fa-snapchat-ghost"></a>
                                <a href="#" class="fs-18 color0 p-r-20 fa fa-youtube-play"></a>
                            </div>
            
                        <div>
                            <p class="s-text7 w-size27">
                                Food orders and tables booking web app
                                Food orders and tables booking web app
                                Food orders and tables booking web app
            
            
                                
                            </p>
                            <a href="#"><i class="fa fa-arrow-right"></i> Read More </a>
            
                            
                        </div>
                    </div>
            
                    <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                        <h4 class="s-text12 p-b-30">
                            Categories
                        </h4>
            
                        <ul>
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    FastFood
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Traditional
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                        Deserts
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Extras
                                </a>
                            </li>
                        </ul>
                    </div>
            
                    <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                        <h4 class="s-text12 p-b-30">
                            Links
                        </h4>
            
                        <ul>
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Search
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    About Us
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Contact Us
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Returns
                                </a>
                            </li>
                        </ul>
                    </div>
            
                    <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                        <h4 class="s-text12 p-b-30">
                            Help
                        </h4>
            
                        <ul>
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Track Order
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Returns
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    Location
                                </a>
                            </li>
            
                            <li class="p-b-9">
                                <a href="#" class="s-text7">
                                    FAQs
                                </a>
                            </li>
                        </ul>
                    </div>
            
                    <div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
                        <h4 class="s-text12 p-b-30">
                            Contact Us
                        </h4>
                        Our Location? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
                    </div>
                </div>
            <!-- BEGIN REGISTER MODAL -->
            <div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Register User</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <h4>Login Form Here</h4>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-primary">Resgiter</button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END REGISTER MODAL -->
              <!-- BEGIN LOGIN MODAL -->
            <div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Login User</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <h4>Login Form Here</h4>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-primary">Login</button>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- END LOGIN MODAL -->
            <!-- cards icons	 -->
              <!-- <div class="t-center p-l-15 p-r-15"> -->
                    <!-- <a href="#">
                        <img class="h-size2" src="images/icons/paypal.png" alt="IMG-PAYPAL">
                    </a>
            
                    <a href="#">
                        <img class="h-size2" src="images/icons/visa.png" alt="IMG-VISA">
                    </a>
            
                    <a href="#">
                        <img class="h-size2" src="images/icons/mastercard.png" alt="IMG-MASTERCARD">
                    </a>
            
                    <a href="#">
                        <img class="h-size2" src="images/icons/express.png" alt="IMG-EXPRESS">
                    </a>
            
                    <a href="#">
                        <img class="h-size2" src="images/icons/discover.png" alt="IMG-DISCOVER">
                    </a> -->
                <!-- </div> -->
            <!-- ends cards icons -->
            
            </footer>
                    <div class="t-center p-t-10 p-b-10 color0 bg-primary">
                        <div class="text-center mb-6 ">
                              <p><span class="font-weight-bold text text-light">POWERED BY FIRMSOLS</span></p>
                            </div>
                        
                    </div>
            
            
            <!-- Back to top -->
            <div class="btn-back-to-top bg0-hov" id="myBtn">
                <span class="symbol-btn-back-to-top">
                    <i class="fa fa-angle-double-up" aria-hidden="true"></i>
                </span>
            </div>
            
            <!-- Container Selection1 -->
            <div id="dropDownSelect1"></div>
            
            
            
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?=base_url('userassets/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?=base_url('userassets/vendor/animsition/js/animsition.min.js')?>"></script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/popper.js')?>"></script>
            <script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?=base_url('userassets/vendor/select2/select2.min.js')?>"></script>
            
            <script type="text/javascript">
                $(".selection-1").select2({
                    minimumResultsForSearch: 20,
                    dropdownParent: $('#dropDownSelect1')
                });
            </script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?=base_url('userassets/vendor/slick/slick.min.js')?>"></script>
            <script type="text/javascript" src="<?=base_url('userassets/js/slick-custom.js')?>"></script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?=base_url('userassets/vendor/countdowntime/countdowntime.js')?>"></script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?=base_url('userassets/vendor/lightbox2/js/lightbox.min.js')?>"></script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="<?base_url('userassets/vendor/sweetalert/sweetalert.min.js')?>"></script>
            
            <script type="text/javascript">
                $('.block2-btn-addcart').each(function(){
                    var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
                    $(this).on('click', function(){
                        swal(nameProduct, "is added to cart !", "success");
                    });
                });
            
                $('.block2-btn-addwishlist').each(function(){
                    var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
                    $(this).on('click', function(){
                        swal(nameProduct, "is added to wishlist !", "success");
                    });
                });
            </script>
            
            <!--===============================================================================================-->
            <script src="<?base_url('userassets/js/home-main.js')?>"></script>
            
            </body>
            </html>