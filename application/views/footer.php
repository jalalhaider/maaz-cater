<!-- Footer -->
	<footer class="p-t-45 p-b-43 p-l-45 p-r-45" style="background-image: url('<?=base_url('userassets/images/footer_img5.jpg')?>'); background-size: cover">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<img src="<?=base_url('userassets/images/logo.png')?>" class="footer-logo" alt="IMG-LOGO">
				</h4>
				<div>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-facebook"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-instagram"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-pinterest-p"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-snapchat-ghost"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-youtube-play"></a>
					</div>

				<div>
					<p class="s-text7 w-size27">
						Food orders and tables booking web app
						Food orders and tables booking web app
						Food orders and tables booking web app


						
					</p>
					<a href="#"><i class="fa fa-arrow-right"></i> Read More </a>

					
				</div>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Categories
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							FastFood
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Traditional
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
								Deserts
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Extras
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Links
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Search
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							About Us
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Contact Us
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Returns
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Help
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Track Order
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Returns
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Location
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							FAQs
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					Contact Us
				</h4>
				Our Location? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
			</div>
		</div>
		<!-- BEGIN LOGIN MODAL -->
		<div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<!-- CUSTOMER_LOGIN -->
		<div class="modal-header modal-heading-sp">
			<div class="form-group col-lg-6 modal-heading"><div class="p-b-7 p-t-7 text-left">LOGIN HERE</div></div>
			<div class="form-group col-lg-6">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
	  </div>		
		<div class="modal-body text-center pull-center modal-spacing" id="login_form">				
					<form>
						<div class="form-row">
							<div class="form-group col-md-12">
									<select class="form-control">
											<option value="" selected="selected">Please Choose Role</option>
											<option value="">Login as Chef</option>
											<option value="">Login as Customer</option>
										  </select>
								</div>

						  <div class="form-group col-md-12">
							<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
						  </div>
						  <div class="form-group col-md-12">
							<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
						  </div>
						  <div class="form-group col-md-4 left-text">
								<a href="#" id="open_forgot_form" >Forgot Password</a>
							  </div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
							  <button type="submit" class="login_btn_color btn-lg btn-block">Log in</button>
							</div>
						  </div>
						</form>		
				</div>
				<!-- END CUSTOMER LOGIN -->
				<!-- RESET PASSWORD -->
				<div class="modal-body text-center pull-center" id="forgot_form">
						<p>Enter your email address</p>
						<form>
							<div class="form-row">
							  <div class="form-group col-md-12">
								<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
							  </div>
							  <div class="form-group col-md-12">
									<p>Already have an account? <a href="#" id="open_login_form"> Login</a> </p>
								  </div>
							</div>
							<div class="form-group row">
								<div class="col-md-12">
								  <button type="submit" class="login_btn_color btn-lg btn-block">Submit</button>
								</div>
							  </div>
							</form>			
					</div>
		<div class="modal-footer">
		  
		</div>
	  </div>
	</div>
  </div>
		<!-- END LOGIN MODAL -->
	  <!-- BEGIN REGISTER MODAL -->

	  <div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			<div style="margin-bottom: 10px;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>			  
					<!-- CUSTOMER REGISTERATION -->
			<div class="modal-body text-center pull-center modal-spacing" id="customer_register_form">  				
					<form>
						<div class="form-row">
							<div class="form-group col-md-7">
								<h5 class="p-b-7 p-t-7 text-left">CUSTOMER REGISTRATION</h5>
							</div>							
							<div class="form-group col-md-5">
							<button type="button" class="login_btn_color btn-block p-b-3 p-l-3" id="open_chef_register_form">BECOME A CHEF</button>
						</div>
						  
						  <div class="form-group col-md-12">
							<input type="text" class="form-control" id="inputEmail4" placeholder="First Name">
						  </div>
						  <div class="form-group col-md-12">
							<input type="text" class="form-control" id="inputPassword4" placeholder="last Name">
						  </div>
						  <div class="form-group col-md-12">
							<input type="email" class="form-control" id="inputPassword4" placeholder="Email">
						  </div>
						  <div class="form-group col-md-12">
							<input type="number" class="form-control" id="inputPassword4" placeholder="Mobile No.">
						  </div>						  
						  <div class="form-group col-md-12">
							<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
						  </div>
						  <div class="form-group col-md-12">
							<input type="password" class="form-control" id="inputPassword4" placeholder="Confirm Password">
						  </div>						  
							<!--  <div class="form-group col-md-12">
									<a href="#" id="open_forgot_form" >Forgot Password</a>
								  </div> -->
						</div>
						<div class="form-group row">
							<div class="col-md-12">
							  <button type="button" class="login_btn_color btn-lg btn-block p-b-3 p-l-3">REGISTER</button>
							</div>
						  </div>
						</form>			
				</div>
				<!-- END CUSTOMER REGISTERATION -->
				<!-- CHEF REGISTERATION -->
				<div class="modal-body text-center pull-center modal-spacing" id="chef_register_form">		
						<form>
							<div class="form-row">
								<div class="form-group col-md-6">
									<h5 class="p-b-7 p-t-7 text-left">CHEF REGISTRATION</h5>
								</div>								
								<div class="form-group col-md-6">
								<button type="button" class="login_btn_color btn-block" id="open_customer_register_form">BECOME A CUSTOMER</button>
							  </div>
							  <div class="form-group col-md-12">
								<input type="text" class="form-control" id="inputEmail4" placeholder="First Name">
							  </div>
							  <div class="form-group col-md-12">
								<input type="text" class="form-control" id="inputPassword4" placeholder="last Name">
							  </div>
							  <div class="form-group col-md-12">
								<input type="email" class="form-control" id="inputPassword4" placeholder="Email">
							  </div>	
							  <div class="form-group col-md-12">
								<input type="number" class="form-control" id="inputPassword4" placeholder="Mobile No.">
							  </div>							  						  
							  <div class="form-group col-md-12">
								<input type="text" class="form-control" id="inputPassword4" placeholder="Business Name">
							  </div>
							  <div class="form-group col-md-12">
								<input type="number" class="form-control" id="inputPassword4" placeholder="Landline No.">
							  </div>							  
							  <div class="form-group col-md-12">
								<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
							  </div>
							  <div class="form-group col-md-12">
								<input type="password" class="form-control" id="inputPassword4" placeholder="Confirm Password">
							  </div>							  
								<!--  <div class="form-group col-md-12">
										<a href="#" id="open_forgot_form" >Forgot Password</a>
									  </div> -->
							</div>
							<div class="form-group row">
								<div class="col-md-12">
								  <button type="submit" class="login_btn_color btn-lg btn-block">REGISTER</button>
								</div>
							  </div>
							</form>			
					</div>
					<!-- END CHEF REGISTERATION -->
					
			<div class="modal-footer">
			  
			</div>
		  </div>
		</div>
	  </div>
	  <!-- END LOGIN MODAL -->
	<!-- cards icons	 -->
	  <!-- <div class="t-center p-l-15 p-r-15"> -->
			<!-- <a href="#">
				<img class="h-size2" src="images/icons/paypal.png" alt="IMG-PAYPAL">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/visa.png" alt="IMG-VISA">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/mastercard.png" alt="IMG-MASTERCARD">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/express.png" alt="IMG-EXPRESS">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/discover.png" alt="IMG-DISCOVER">
			</a> -->
		<!-- </div> -->
	<!-- ends cards icons -->
	
	</footer>
			<div class="t-center p-t-10 p-b-10 color0 bg-primary">
				<div class="text-center mb-6 ">
					  <p><span class="font-weight-bold text text-light">POWERED BY FIRMSOLS</span></p>
					</div>
				
			</div>


	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/animsition/js/animsition.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/popper.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/select2/select2.min.js')?>"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/userassets/vendor/slick/slick.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('userassets/js/slick-custom.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/countdowntime/countdowntime.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/lightbox2/js/lightbox.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/sweetalert/sweetalert.min.js')?>"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?=base_url('userassets/js/home-main.js')?>"></script>