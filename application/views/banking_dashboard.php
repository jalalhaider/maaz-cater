<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    <title>Dish Area | Chef Dashboard</title>
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url('userassets/css/bootstrap.min2.css')?>" rel="stylesheet">
    <link href="<?=base_url('userassets/css/font-awesome.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('userassets/css/animsition.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('userassets/css/animate.css')?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url('userassets/css/style.css')?>" rel="stylesheet">

    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="<?=base_url('userassets/images/icons/favicon.png')?>" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/bootstrap/css/bootstrap.min.css')?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/themify/themify-icons.css')?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/elegant-font/html-css/style.css')?>">
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">-->
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> -->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/animsition/css/animsition.min.css')?>">
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">-->
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css"> -->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/slick/home-slick.css')?>">
    <!--===============================================================================================
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css"> -->
    <!--===============================================================================================-->

    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/bootstrap/css/bootstrap.min.css')?>">

    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-util.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-main.css')?>">
    <!--===============================================================================================-->
    <!-- CARGO_CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-style.css')?>">
    <!-- MY_CSS -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-mystyle.css')?>">
    <!-- LOGO CAROUSEL JAVA -->
    <link href="<?=base_url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css')?>" rel="stylesheet" id="bootstrap-css">
    <script src="<?=base_url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js')?>"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
           $("#all_dishes").show();
          $("#all_dishes_heading").show();

           $("#add_new_dishes_heading").hide();
           $("#add_new_dishes").hide();

         $("#new_dish_btn").click(function(){

           $("#all_dishes_heading").hide(500);
           $("#all_dishes").hide(500);
           
           $("#add_new_dishes").show(200);
           $("#add_new_dishes_heading").show(200);
         });

         $("#all_dishes_btn").click(function(){

           $("#all_dishes").show(200);
           $("#all_dishes_heading").show(200);

           $("#add_new_dishes").hide(200);
           $("#add_new_dishes_heading").hide(200);
         });
         
        //  adding_ingredients
        $("#btn1").click(function(){
 var item_val =  $("#item1").val();
    $("#drop1").append(" <option>"+item_val+"</option>");
    $("#item1").val('');
  });

//   remove ingredient
$("#btn2").click(function(){
 var item_val =  $("#drop1").val();
 $("#drop1 option:selected").remove();
   // $('#drop1 option:selected').removeAttr('selected');
  });

 
       });
       </script>
    <style type="text/css">
        .container {
    margin-top: 2%;
}

#all_dishes {
    padding-top: 2%;
}

.contact-page.dash-nav {
    margin-top: -12%;
}

#all_dishes {
    margin-bottom: 8%;
}
    </style>
</head>

<body>
    <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
        <!-- Header -->
        <header>
            <!-- Header desktop -->
            <div class="container-menu-header">
                <!-- <div class="topbar">
                <div class="topbar-social">
                    <a href="#" class="topbar-social-item fa fa-facebook"></a>
                    <a href="#" class="topbar-social-item fa fa-instagram"></a>
                    <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                    <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                    <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                </div>

                <span class="topbar-child1">
                    Free shipping for standard order over $100
                </span>

                <div class="topbar-child2">
                    <span class="topbar-email">
                        fashe@example.com
                    </span>

                    <div class="topbar-language rs1-select2">
                        <select class="selection-1" name="time">
                            <option>USD</option>
                            <option>EUR</option>
                        </select>
                    </div>
                </div>
            </div> -->

                <div class="wrap_header site-navbar">
                    <!-- Logo -->
                    <a href="<?=base_url('index')?>" class="logo">
                        <img src="<?=base_url('userassets/images/logo.png')?>" alt="IMG-LOGO">
                    </a>

                    <!-- Menu -->
                    <!-- <div class="wrap_menu ">
                    <nav class="menu">
                        <ul class="main_menu">

                            <li>
                                <a href="about.html" class="shadow p-3 mb-5">Become a Chef</a>
                            </li>

                            <li>
                                <a href="contact.html" class="shadow p-3 mb-5">Register User</a>
                            </li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-12">
                        <nav class="site-navigation text-right ml-auto " role="navigation">

                            <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                                <?php
                                    if($this->session->userdata('id'))
                                    {
                                ?>
                                <li><a href="jump_to_customer" onclick="return confirm('Are you sure you want to go to customer?');" class="nav-link">Switch to Customer</a></li>
                <li class="header-wrapicon2"><a href="#" class="nav-link js-show-header-dropdown">My Account</a>
                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">
                            <ul>
                                <li class="header-cart-item custom-drop">
                                    <div class="header-cart-item">
                                        <strong class="header-h1"><i class="fa fa-check"></i></strong> 
                                    </div>                              
                                </li>
                                <li class="header-cart-item custom-drop">
                                <a href="<?=base_url('dashboard')?>" class="custom-drop-a"><i class="fa fa-tachometer"></i> Dashboad</a>
                                </li>
                                <li class="header-cart-item custom-drop">
                                <a href="<?=base_url('profile_data')?>" class="custom-drop-a"> <i class="fa fa-user"></i> Profile</a>
                                </li>
                                <li class="header-cart-item custom-drop">
                                <a href="#" class="custom-drop-a"><i class="fa fa-phone"></i> Help & Support</a>
                                </li>                               
                                <li class="header-cart-item custom-drop" style="border-bottom: 0;">
                                <a href="<?=base_url('UserController/logout')?>" class="custom-drop-a"><i class="fa fa-sign-out"></i> Logout</a>
                                </li>                               

                            </ul>
                        </div>
                  </li>   
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                    <li><a href="#home-section" class="nav-link" data-toggle="modal"
                                        data-target="#registermodal">Register</a></li>
                                    
                                <?php 
                                    }                            
                                ?>
                            </ul>
                        </nav>
                    </div>
                    <!-- Header Icon -->

                </div>
            </div>

            
            <!-- Header Mobile -->
            <div class="wrap_header_mobile">
                <!-- Logo moblie -->
                <a href="<?=base_url('index')?>" class="logo-mobile">
                    <img src="<?=base_url('userassets/images/icons/logo.png')?>" alt="IMG-LOGO">
                </a>

                <!-- Button show menu -->
                <div class="btn-show-menu">
                    <!-- Header Icon mobile -->
                    <div class="header-icons-mobile">
                        <a href="#" class="header-wrapicon1 dis-block">
                            <img src="<?=base_url('userassets/images/icons/icon-header-01.png')?>" class="header-icon1" alt="ICON">
                        </a>

                        <span class="linedivide2"></span>

                        <div class="header-wrapicon2">
                            <img src="<?=base_url('userassets/images/icons/icon-header-02.png')?>" class="header-icon1 js-show-header-dropdown"
                                alt="ICON">
                            <span class="header-icons-noti">0</span>

                            <!-- Header cart noti -->
                            <div class="header-cart header-dropdown">
                                <ul class="header-cart-wrapitem">
                                    <li class="header-cart-item">
                                        <div class="header-cart-item-img">
                                            <img src="<?=base_url('userassets/images/item-cart-01.jpg')?>" alt="IMG">
                                        </div>

                                        <div class="header-cart-item-txt">
                                            <a href="#" class="header-cart-item-name">
                                                White Shirt With Pleat Detail Back
                                            </a>

                                            <span class="header-cart-item-info">
                                                1 x $19.00
                                            </span>
                                        </div>
                                    </li>

                                    <li class="header-cart-item">
                                        <div class="header-cart-item-img">
                                            <img src="<?=base_url('userassets/images/item-cart-02.jpg')?>" alt="IMG">
                                        </div>

                                        <div class="header-cart-item-txt">
                                            <a href="#" class="header-cart-item-name">
                                                Converse All Star Hi Black Canvas
                                            </a>

                                            <span class="header-cart-item-info">
                                                1 x $39.00
                                            </span>
                                        </div>
                                    </li>

                                    <li class="header-cart-item">
                                        <div class="header-cart-item-img">
                                            <img src="<?=base_url('userassets/images/item-cart-03.jpg')?>" alt="IMG">
                                        </div>

                                        <div class="header-cart-item-txt">
                                            <a href="#" class="header-cart-item-name">
                                                Nixon Porter Leather Watch In Tan
                                            </a>

                                            <span class="header-cart-item-info">
                                                1 x $17.00
                                            </span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="header-cart-total">
                                    Total: $75.00
                                </div>

                                <div class="header-cart-buttons">
                                    <div class="header-cart-wrapbtn">
                                        <!-- Button -->
                                        <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                            View Cart
                                        </a>
                                    </div>

                                    <div class="header-cart-wrapbtn">
                                        <!-- Button -->
                                        <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                            Check Out
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </div>
                </div>
            </div>

            <!-- Menu Mobile -->
            <div class="wrap-side-menu">
                <nav class="side-menu">
                    <ul class="main-menu">
                        <!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                        <span class="topbar-child1">
                            Free shipping for standard order over $100
                        </span>
                    </li> -->

                        <!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                        <div class="topbar-child2-mobile">
                            <span class="topbar-email">
                                fashe@example.com
                            </span>

                            <div class="topbar-language rs1-select2">
                                <select class="selection-1" name="time">
                                    <option>USD</option>
                                    <option>EUR</option>
                                </select>
                            </div>
                        </div>
                    </li> -->

                        <li class="item-topbar-mobile p-l-10">
                            <div class="topbar-social-mobile">
                                <a href="#" class="topbar-social-item fa fa-facebook"></a>
                                <a href="#" class="topbar-social-item fa fa-instagram"></a>
                                <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                                <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                                <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                            </div>
                        </li>




                        <li class="item-menu-mobile">
                            <a href="about.html">Become a Chef</a>
                        </li>

                        <li class="item-menu-mobile">
                            <a href="contact.html">Register User</a>
                        </li>
                        <li class="item-menu-mobile">
                            <a href="contact.html">0800 1234567</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="page-wrapper">
            <div class="text-center mb-6 heading-spacer">
                <div class="block-heading-1 ">
                    <h2><span>Banking</span> Section</h2>
                </div>
            </div>
            <!-- DASHBOARD NAVS -->
            <section class="contact-page inner-page">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 pull-center">
                            <nav class=" navbar navbar-expand-lg navbar-dark bg-dark justify-content-md-center">
                                <div class=" justify-content-md-center" id="navbarsExample08">
                                    <ul class="dashboard-navbar">
                                        <li class="dashboard-nav-item">
                                            <a href="<?=base_url('dashboard')?>">Dishes</a>
                                        </li>
                                        <li class="dashboard-nav-item">
                                            <a class="dashboard-nav-link " href="<?=base_url('coupons')?>">Coupons</a>
                                        </li>

                                        <li class="dashboard-nav-item">
                                            <a class="dashboard-nav-link " href="#">Orders</a>
                                        </li>

                                        <li class="dashboard-nav-item">
                                             <a class="dashboard-nav-link" href="<?=base_url('profile_data')?>">Account Settings</a>
                                             
                                        </li>

                                        <li class="dashboard-nav-item">
                                            <a class="dashboard-nav-link" href="<?=base_url('stages')?>">Stages</a>
                                        </li>

                                        <li class="dashboard-nav-item">
                                            <a class="dashboard-nav-link" href="<?=base_url('banking_dashboard')?>">Banking</a>
                                        </li>

                                        <li class="dashboard-nav-item">
                                            <a class="dashboard-nav-link" href="#">Transactions</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </section>
        </div>

<!-- Input Fields -->

 <div class="page-wrapper">
            <script type="text/javascript"> 
                 function show1() {
                document.getElementById("new_dish_btn").style.display = "block";
                document.getElementById("all_dishes_btn").style.display = "none";
            }
            window.onclick = function(event) {
                if (!event.target.matches('.button1'))
                    var dropdowns = document.getElementsByClassName("div1_pop");
            }
            function show2() {
                document.getElementById("new_dish_btn").style.display = "none";
                document.getElementById("all_dishes_btn").style.display = "block";
            }
            window.onclick = function(event) {
                if (!event.target.matches('.button2'))
                var dropdowns = document.getElementsByClassName("div2_pop");
            }
                </script>
            <section class="contact-page dash-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 pull-center">
                            <div class="text-right pull-right dash-btn">
                                <button class="btn btn-warning" id="new_dish_btn" onclick="show2()">Add Bank details</button>
                                <button class="btn btn-warning" id="all_dishes_btn" onclick="show1()">All Details</button>
                            </div>
                            <!-- STARTS ADD NEW DISH FORM -->
                            <div class="text-center mb-6 up_spacer" id="add_new_dishes_heading">
                                <div class="sub-h3">
                                    <h3><span>All</span> Details</h3>
                                   
                                </div>
                            </div>
                        <form method="post" action="UserController/add_bank_details">
                            <center><b style="color: red;">
                                <?php echo $this->session->userdata('msg');?>
                                    </b>
                            </center>                           
                            <div id="add_new_dishes">
                                <!-- form sect 1 -->
                                <div class="row up_spacer">
                                    
                        <div class="form-group col-xs-2 col-sm-3 col-md-3">
                            <select class="form-control" name="country" required="">
                            <option>Select Country</option>
                                <?php foreach($countries as $value){?>
                                <option value="<?=$value->Country?>"><?=$value->Country?></option>
                               <?php }?>
                            </select>
                        </div>

                                    
                        <div class="form-group col-xs-2 col-sm-3 col-md-3">
                            <select class="form-control" name="state" required="">
                                <option>Select State</option>
                                    
                                <?php foreach($states as $value){?>
                            <option value="<?=$value->state?>"><?=$value->state?></option>
                               <?php }?>
                                
                            </select>
                        </div>

                        <div class="form-group col-xs-2 col-sm-3 col-md-3">
                            <select class="form-control" name="city" required="">
                                <option>Select City</option>
                                    
                                <?php foreach($cities as $value){?>
                                <option value="<?=$value->city?>"><?=$value->city?></option>
                               <?php }?>
                                
                            </select>
                        </div>

                        <div class="form-group col-xs-2 col-sm-3 col-md-3">
                            <select class="form-control" name="bank_name" required="">
                                <option>Select Bank</option>
                                <option>UBL</option>
                                <option>HBL</option>
                                <option>NBP</option>
                                <option>FINCA BANK</option>
                                <option>BANK ALFALAF</option>
                            </select>
                        </div>
  
                                <!-- form sect 2 -->
                                
                        <div class="form-group col-sm-3">
                            <input type="text" name="account_title" class="form-control" placeholder="Add Account title">
                        </div>

                        <div class="form-group col-sm-3">
                            <input type="number" name="account_number" class="form-control" placeholder="Add Account Number">
                        </div>

                        <div class="form-group col-sm-3">
                            <input type="text" name="iban_number" class="form-control" placeholder="Add IBAN Number">
                        </div>

                        <div class="form-group col-sm-3">
                            <input type="text" name="branch_code" class="form-control" placeholder="Add Branch Code">
                        </div>

                        <div class="form-group col-sm-3">
                            <input type="text" name="branch_name" class="form-control" placeholder="Add Branch Name">
                        </div>

                                </div>
                                
                        <div>
                            <div class="form-group col-sm-12">
                                <input type="submit" class="btn btn-warning btn-lg" value="Save it all">
                            </div>
                        </div>
                                
                            </div>
                      

                        </form>
                           
                            <div id="all_dishes">
                                <div class="text-center mb-6 up_spacer" id="all_dishes_heading">
                                    <div class="sub-h3">
                                        <h3><span>All</span> DETAILS</h3>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table id="mytable" class="table table-bordred table-striped">
        
                                        <thead>
        
                                            <tr>
                                                <th>Bank Name</th>
                                                <th>Account Title</th>
                                                <th>Account Number</th>
                                                <th>IBAN Number</th>
                                                <th>Branch Code</th>
                                                <th>Branch Name</th>
                                                <th>Actions</th>
                                                <th>Primary</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <script type="text/javascript">
                                            function chekbox()
                                            {
                                                //alert('chekbox');
                                               var myOrderString =  document.getElementById('chekbox').value;
                                              alert(myOrderString);
                                               

                                               

                                            }
                                            
                                        </script>
                                        <?php foreach($bank as $value){ ?>
                                            <tr>
                                                
                                                <td><?php echo $value->bank_name;?></td>
                                                <td><?php echo $value->account_title;?></td>
                                                <td><?php echo $value->account_number;?></td>
                                                <td><?php echo $value->iban_number;?></td>
                                                <td><?php echo $value->branch_code;?></td>
                                                <td><?php echo $value->branch_name;?></td>
                                                <td>
                                                    
                                                    <p data-placement="top" data-toggle="tooltip" title="Action">

<!-- <?php echo anchor('UserController/edit_dish/'.$value->id, ' ',['class'=>'fa fa-edit']);?> -->

<?php echo anchor('UserController/dlt_bank/'.$value->id, ' ',['class'=>'fa fa-trash']);?>

                                                    </p>
                                                </td>
                                                <td >
                                                   
                                                  

                                            <div id="checkbox-container">
                                              <div>
                                                <label for="option1">Option 1</label>
                                                <input type="checkbox" id="chekbox" value="<?php echo $value->checked;?>" onclick="chekbox()">
                                              </div>
                                             
                                            </div>
                                                        
                                                </td>
                                            </tr>
                                            
                                        <?php } ?>
        
                                        </tbody>
        
                                    </table>
        
                                    
        
        
                                </div>
                            </div>
                            <!-- ENDS STARTS ALL DISHES -->
                        </div>
                    </div>
                </div>
            </section>
        </div>

<!-- ======================== -->

        <!-- start: FOOTER -->
        <footer class=" p-t-45 p-b-43 p-l-45 p-r-45"
            style="background-image: url('<?=base_url('userassets/images/footer_img5.jpg')?>'); background-size: cover">
            <div class="flex-w p-b-90">
                <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
                    <h4 class="s-text12 p-b-30">
                        <img src="<?=base_url('userassets/images/logo.png')?>" class="footer-logo" alt="IMG-LOGO">
                    </h4>
                    <div>
                        <a href="#" class="fs-18 color0 p-r-20 fa fa-facebook"></a>
                        <a href="#" class="fs-18 color0 p-r-20 fa fa-instagram"></a>
                        <a href="#" class="fs-18 color0 p-r-20 fa fa-pinterest-p"></a>
                        <a href="#" class="fs-18 color0 p-r-20 fa fa-snapchat-ghost"></a>
                        <a href="#" class="fs-18 color0 p-r-20 fa fa-youtube-play"></a>
                    </div>

                    <div>
                        <p class="s-text7 w-size27">
                            Food orders and tables booking web app
                            Food orders and tables booking web app
                            Food orders and tables booking web app



                        </p>
                        <a href="#"><i class="fa fa-arrow-right"></i> Read More </a>


                    </div>
                </div>

                <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                    <h4 class="s-text12 p-b-30">
                        Categories
                    </h4>

                    <ul>
                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                FastFood
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Traditional
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Deserts
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Extras
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                    <h4 class="s-text12 p-b-30">
                        Links
                    </h4>

                    <ul>
                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Search
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                About Us
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Contact Us
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Returns
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                    <h4 class="s-text12 p-b-30">
                        Help
                    </h4>

                    <ul>
                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Track Order
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Returns
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                Location
                            </a>
                        </li>

                        <li class="p-b-9">
                            <a href="#" class="s-text7">
                                FAQs
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
                    <h4 class="s-text12 p-b-30">
                        Contact Us
                    </h4>
                    Our Location? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on
                    (+1) 96 716 6879
                </div>
            </div>
            <!-- BEGIN REGISTER MODAL -->
            <div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Register User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4>Login Form Here</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary">Resgiter</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END REGISTER MODAL -->
            <!-- BEGIN LOGIN MODAL -->
            <div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Login User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4>Login Form Here</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary">Login</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- END LOGIN MODAL -->
            <!-- dish title modal -->
            <div class="modal fade" id="dishtitle" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modal Header</h4>
                        </div>
                        <div class="modal-body">
                            <p>Some text in the modal.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- cards icons     -->
            <!-- <div class="t-center p-l-15 p-r-15"> -->
            <!-- <a href="#">
                       <img class="h-size2" src="images/icons/paypal.png" alt="IMG-PAYPAL">
                   </a>
           
                   <a href="#">
                       <img class="h-size2" src="images/icons/visa.png" alt="IMG-VISA">
                   </a>
           
                   <a href="#">
                       <img class="h-size2" src="images/icons/mastercard.png" alt="IMG-MASTERCARD">
                   </a>
           
                   <a href="#">
                       <img class="h-size2" src="images/icons/express.png" alt="IMG-EXPRESS">
                   </a>
           
                   <a href="#">
                       <img class="h-size2" src="images/icons/discover.png" alt="IMG-DISCOVER">
                   </a> -->
            <!-- </div> -->
            <!-- ends cards icons -->

        </footer>
        <div class="t-center p-t-10 p-b-10 color0 bg-primary">
            <div class="text-center mb-6 ">
                <p><span class="font-weight-bold text text-light">POWERED BY FIRMSOLS</span></p>
            </div>

        </div>


        <!-- Back to top -->
        <div class="btn-back-to-top bg0-hov" id="myBtn">
            <span class="symbol-btn-back-to-top">
                <i class="fa fa-angle-double-up" aria-hidden="true"></i>
            </span>
        </div>

        <!-- Container Selection1 -->
        <div id="dropDownSelect1"></div>



        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script type="text/javascript" src="<?=base_url('userassets/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
        <!--===============================================================================================-->
        <script type="text/javascript" src="<?=base_url('userassets/vendor/animsition/js/animsition.min.js')?>"></script>
        <!--=======================================================================================-->
        <script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/popper.js')?>"></script>
         <script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
        <!--===============================================================================-->
        <script type="text/javascript" src="<?=base_url('userassets/vendor/select2/select2.min.js')?>"></script>
        <script type="text/javascript">
            $(".selection-1").select2({
                minimumResultsForSearch: 20,
                dropdownParent: $('#dropDownSelect1')
            });
        </script>
        <!--==============================================================================-->
        <script type="text/javascript" src="<?=base_url('userassets/vendor/slick/slick.min.js')?>"></script>
        <script type="text/javascript" src="<?=base_url('userassets/js/slick-custom.js')?>"></script>
        <!--====================================================================================-->
         <script type="text/javascript" src="<?=base_url('userassets/vendor/countdowntime/countdowntime.js')?>"></script>
        <!--====================================================================================-->
        <script type="text/javascript" src="<?=base_url('userassets/vendor/lightbox2/js/lightbox.min.js')?>"></script>
        <!--===================================================================================-->
        <script type="text/javascript" src="<?=base_url('userassets/vendor/sweetalert/sweetalert.min.js')?>"></script>

        <script type="text/javascript">
            $('.block2-btn-addcart').each(function () {
                var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
                $(this).on('click', function () {
                    swal(nameProduct, "is added to cart !", "success");
                });
            });

            $('.block2-btn-addwishlist').each(function () {
                var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
                $(this).on('click', function () {
                    swal(nameProduct, "is added to wishlist !", "success");
                });
            });
        </script>

        <!--====================================================================================-->
        <script src="<?=base_url('userassets/js/home-main.js')?>"></script>

</body>

</html>