<?php
/*echo "<pre>";
print_r($dishes);
die();*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?=$title?></title>
<script src="<?=base_url('userassets/js/lib.js')?>"></script>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?=base_url('userassets/images/icons/favicon.png')?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/bootstrap/css/bootstrap.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/themify/themify-icons.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/fonts/elegant-font/html-css/style.css')?>">
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">--> 
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/animsition/css/animsition.min.css')?>">
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">-->
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/vendor/slick/home-slick.css')?>">
<!--===============================================================================================
	<link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-util.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-main.css')?>">
<!--===============================================================================================-->
<!-- CARGO_CSS -->
<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-style.css')?>">
<!-- MY_CSS -->
<link rel="stylesheet" type="text/css" href="<?=base_url('userassets/css/home-mystyle.css')?>">
<!-- LOGO CAROUSEL JAVA <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script> -->

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<script>
        $(document).ready(function(){
		   
		   $("#chef_register_form").hide();
		   $("#customer_register_form").show();
		   $("#forgot_form").hide();
		   $("#login_form").show();

         $("#open_customer_register_form").click(function(){

			$("#customer_register_form").show(200);
			$("#chef_register_form").hide();
		   $("#forgot_form").hide(200);

         });
		$("#open_chef_register_form").click(function(){

			$("#chef_register_form").show(200);
			$("#customer_register_form").hide(200);
		   $("#forgot_form").hide(200);

         });

		//  forgot_form
		 $("#open_forgot_form").click(function(){
			
			$("#forgot_form").show(200);
			$("#login_form").hide(200);

		 });
		 
		 $("#open_login_form").click(function(){

		$("#login_form").show(200);
		$("#forgot_form").hide(200);


});
       });
       </script>
<body class="animsition">

<header>
		<!-- Header desktop -->
		<div class="container-menu-header">

			<div class="wrap_header site-navbar">
				<!-- Logo -->
				<a href="<?=base_url('index')?>" class="logo">
						<img src="<?=base_url('userassets/images/logo.png')?>" alt="IMG-LOGO">
				</a>
				<?php $this->session->flashdata('msg');?>
				<div class="col-12">
              <nav class="site-navigation text-right ml-auto " role="navigation">

                <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                 
                 	<?php if($this->session->userdata('id') and $this->session->userdata('cus')) { ?>
                 	 <li class="header-wrapicon2"><a href="#" class="nav-link js-show-header-dropdown">My Account</a>
						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
							<ul>
								<li class="header-cart-item custom-drop">
									<div class="header-cart-item">
										<strong class="header-h1"><i class=""></i></strong>	
									</div>								
								</li>
								<li class="header-cart-item custom-drop">
								<a href="<?=base_url('user_dashboard')?>" class="custom-drop-a"><i class="fa fa-tachometer"></i> Dashboad</a>
								</li>
								<li class="header-cart-item custom-drop">
								<a href="#" class="custom-drop-a"> <i class="fa fa-user"></i> Profile</a>
								</li>
								<li class="header-cart-item custom-drop">
								<a href="#" class="custom-drop-a"><i class="fa fa-phone"></i> Help & Support</a>
								</li>								
								<li class="header-cart-item custom-drop" style="border-bottom: 0;">
								<a href="<?=base_url('UserController/logout')?>" class="custom-drop-a"><i class="fa fa-sign-out"></i> Logout</a>
								</li>								

							</ul>
						</div>
                  </li>
                               

                 	<?php } else { ?>
                  <li><a href="#home-section" class="nav-link" data-toggle="modal" data-target="#registermodal">Register</a></li>
                  <li><a href="#services-section" class="nav-link" data-toggle="modal" data-target="#loginmodal">Login</a></li>
                 	<?php } ?> 

				  <!-- <li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
					  aria-haspopup="true" aria-expanded="false">Dropdown</a>
					<div class="dropdown-menu dropdown-primary col-md-12" aria-labelledby="navbarDropdownMenuLink">
						<div class="row">
							<div class="col-sm-12">
								<a class="dropdown-item" href="#"> <img src="images/person_1.jpg" style="width: 100px; height: 100px;" alt=""> </a>
							</div>
						<div class="row">
							<div class="col-xs-6"> <i class="fa fa-user"></i></div>
								<div class="col-xs-6"><a class="dropdown-item" href="#">Profile</a></div>
						</div>
						<div class="row">
							<div class="col-xs-6"><i class="fa fa-settings"></i></div>
							<div class="col-xs-6"><a class="dropdown-item" href="#">Settings</a></div>
						</div>
						<div class="row">
							<div class="col-xs-6"><i class="fa fa-logout"></i></div>
							<div class="col-xs-6"><button class="dropdown-item">Log Out</button></div>
						</div>
							
						</div>
						
					</div>
				  </li> -->
				  
				  
				</ul>
				
              </nav>

            </div>

				<!-- Header Icon -->
				
			</div>
		</div>

		<!-- Header Mobile -->
		<div class="wrap_header_mobile">
			<!-- Logo moblie -->
			<a href="<?=base_url('index')?>" class="logo-mobile">
				<img src="<?=base_url('userassets/images/icons/logo.png')?>" alt="IMG-LOGO">
			</a>

			<!-- Button show menu -->
			<div class="btn-show-menu">
				<!-- Header Icon mobile -->
				<div class="header-icons-mobile">
					<a href="#" class="header-wrapicon1 dis-block">
						<img src="<?=base_url('userassets/images/icons/icon-header-01.png')?>" class="header-icon1" alt="ICON">
					</a>

					<span class="linedivide2"></span>

					<div class="header-wrapicon2">
						<img src="<?=base_url('userassets/images/icons/icon-header-02.png')?>" class="header-icon1 js-show-header-dropdown" alt="ICON">
						<span class="header-icons-noti">0</span>

						<!-- Header cart noti -->
						<div class="header-cart header-dropdown">
							<ul class="header-cart-wrapitem">
								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?=base_url('userassets/images/item-cart-01.jpg')?>" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="#" class="header-cart-item-name">
											White Shirt With Pleat Detail Back
										</a>

										<span class="header-cart-item-info">
											1 x $19.00
										</span>
									</div>
								</li>

								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?=base_url('userassets/images/item-cart-02.jpg')?>" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="#" class="header-cart-item-name">
											Converse All Star Hi Black Canvas
										</a>

										<span class="header-cart-item-info">
											1 x $39.00
										</span>
									</div>
								</li>

								<li class="header-cart-item">
									<div class="header-cart-item-img">
										<img src="<?=base_url('userassets/images/item-cart-03.jpg')?>" alt="IMG">
									</div>

									<div class="header-cart-item-txt">
										<a href="#" class="header-cart-item-name">
											Nixon Porter Leather Watch In Tan
										</a>

										<span class="header-cart-item-info">
											1 x $17.00
										</span>
									</div>
								</li>
							</ul>

							<div class="header-cart-total">
								Total: $75.00
							</div>

							<div class="header-cart-buttons">
								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										View Cart
									</a>
								</div>

								<div class="header-cart-wrapbtn">
									<!-- Button -->
									<a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
										Check Out
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>
		</div>

		<!-- Menu Mobile -->
		<div class="wrap-side-menu" >
			<nav class="side-menu">
				<ul class="main-menu">
					<!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							Free shipping for standard order over $100
						</span>
					</li> -->

					<!-- <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<div class="topbar-child2-mobile">
							<span class="topbar-email">
								fashe@example.com
							</span>

							<div class="topbar-language rs1-select2">
								<select class="selection-1" name="time">
									<option>USD</option>
									<option>EUR</option>
								</select>
							</div>
						</div>
					</li> -->

					<li class="item-topbar-mobile p-l-10">
						<div class="topbar-social-mobile">
							<a href="#" class="topbar-social-item fa fa-facebook"></a>
							<a href="#" class="topbar-social-item fa fa-instagram"></a>
							<a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
							<a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
							<a href="#" class="topbar-social-item fa fa-youtube-play"></a>
						</div>
					</li>

					


					<li class="item-menu-mobile">
						<a href="about.html">Become a Chef</a>
					</li>

					<li class="item-menu-mobile">
						<a href="contact.html">Register User</a>
					</li>
					
				</ul>
			</nav>
		</div>
	</header>	

	<!-- Slide1 -->
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script>
function showPosition() {
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showMap, showError);
    } else {
        alert("Sorry, your browser does not support HTML5 geolocation.");
    }
}
 
// Define callback function for successful attempt
function showMap(position) {
    // Get location data
    lat = position.coords.latitude;
    long = position.coords.longitude;
    var latlong = new google.maps.LatLng(lat, long);
    
    var myOptions = {
        center: latlong,
        zoom: 16,
        mapTypeControl: true,
        navigationControlOptions: {
            style:google.maps.NavigationControlStyle.SMALL
        }
    }
    
    var map = new google.maps.Map(document.getElementById("embedMap"), myOptions);
    var marker = new google.maps.Marker({ position:latlong, map:map, title:"You are here!" });
}
 
// Define callback function for failed attempt
function showError(error) {
    if(error.code == 1) {
        result.innerHTML = "You've decided not to share your position, but it's OK. We won't ask you again.";
    } else if(error.code == 2) {
        result.innerHTML = "The network is down or the positioning service can't be reached.";
    } else if(error.code == 3) {
        result.innerHTML = "The attempt timed out before it could get the location data.";
    } else {
        result.innerHTML = "Geolocation failed due to unknown error.";
    }
}
</script>
	<div class="ftco-blocks-cover-1">
			<div class="ftco-cover-1 overlay" style="background-image: url(userassets/images/uCatered.jpg)">
			  <div class="container">
				<div class="row align-items-center">
				  <div class="col-lg-6" style="text-align: center;margin: 0 auto;">
					<h1><span>food</span> on fingers</h1>
					<form action="#">
					  <div class="form-group d-flex">
					  	 <button type="button" onclick="showPosition();"></button>
    						<div id="embedMap" style="width: 400px; height: 300px;">
        						<!--Google map will be embedded here-->
    							
    						<input type="text" class="form-control" placeholder="Enter your Delivery Address">
    						</div>
						
						<!--<input type="submit" class="btn btn-primary text-white px-4" value="SEARCH"> -->
						<button class="btn btn-primary text-white px-4" onclick="window.location.href = 'chef_searchby.html';">SEARCH</button>
					  </div>
					</form>
				  </div>
				</div>
			  </div>
			</div>
		   
		  </div>

	<!-- BEGINS WORLDWIDE DISHES CAROUSEL -->
	<section class="newproduct bgwhite p-t-45 p-b-105">
		<div class="container">
				<div class="text-center mb-6 up_down_spacer">
						<div class="block-heading-1 ">
						  <h2><span>WORLDWIDE</span> DISHES</h2>
						</div>
					  </div>

			<!-- Slide2 -->
			<div class="wrap-dishes" onmouseover="stop();" onmousemove="start();">
				<div class="dishes">
<?php 


									if(isset($dishes) )
									{
										foreach ($dishes as  $value) {

											
?>
					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="wrap-pic-w">
								<img src="<?=base_url('assets/images/'.$value->dish_image)?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
							</div>

							<div class="block2-txt p-t-20">
								<a href="product-detail.html" class="carousel_font font-weight-bold text-primary block2-name dis-block text-center p-b-5">
									<?php
									print_r($value->region);
									//print_r($value->region);
									?>
									
								</a>
							</div>
						</div>
					</div>
					<?php
										}
									}
							?>
				</div>
			</div>

							<div class="seemore-btn"><a href="#" class="seemore-btn">see more</a></div>
		</div>
	</section>
	<!-- ENDS WORLDWIDE CAROUSEL -->

	<!-- BEGINS CHEF CAROUSEL 1 -->
	<section class="newproduct bgwhite p-t-45 p-b-105 chef_container">
			<div class="container">
					<div class="text-center mb-6 up_down_spacer">
							<div class="block-heading-1 ">
							  <h2><span>FEATURED</span> CHEF</h2>
							</div>
						  </div>
	
				<!-- Slide2 -->
				<div class="wrap-chef1 " onmouseover="stop();" onmousemove="start();">
					<div class="chef1">
	
						<div class="item-slick2 p-l-15 p-r-15">
							<!-- Block2 -->
							<div class="block2">
								<div class="wrap-pic-w">
									<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
								</div>
	
								<div class="block2-txt p-t-20">
									<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
										Thoma Balfor
									</a>
								</div>
							</div>
						</div>
						<div class="item-slick2 p-l-15 p-r-15">
								<!-- Block2 -->
								<div class="block2">
									<div class="wrap-pic-w">
										<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
									</div>
		
									<div class="block2-txt p-t-20">
										<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
											Thoma Balfor
										</a>
									</div>
								</div>
							</div>
							<div class="item-slick2 p-l-15 p-r-15">
									<!-- Block2 -->
									<div class="block2">
										<div class="wrap-pic-w">
											<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
										</div>
			
										<div class="block2-txt p-t-20">
											<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
												Thoma Balfor
											</a>
										</div>
									</div>
								</div>
								<div class="item-slick2 p-l-15 p-r-15">
										<!-- Block2 -->
										<div class="block2">
											<div class="wrap-pic-w">
												<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
											</div>
				
											<div class="block2-txt p-t-20">
												<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
													Thoma Balfor
												</a>
											</div>
										</div>
									</div>
									<div class="item-slick2 p-l-15 p-r-15">
											<!-- Block2 -->
											<div class="block2">
												<div class="wrap-pic-w">
													<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
												</div>
					
												<div class="block2-txt p-t-20">
													<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
														Thoma Balfor
													</a>
												</div>
											</div>
										</div>
										<div class="item-slick2 p-l-15 p-r-15">
												<!-- Block2 -->
												<div class="block2">
													<div class="wrap-pic-w">
														<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
													</div>
						
													<div class="block2-txt p-t-20">
														<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
															Thoma Balfor
														</a>
													</div>
												</div>
											</div>
					</div>
				</div>
	<div class="seemore-btn"><a href="#" class="seemore-btn">see more</a></div>
			</div>
		</section>
	<!-- ENDS CHEF CAROUSEL 1 -->	
		<!-- BEGINS DISHES GALLERY -->
		<section class="newproduct bgwhite p-t-45 p-b-105">
			<div class="container">
					<div class="text-center mb-6 up_down_spacer">
							<div class="block-heading-1 ">
							  <h2><span>TOP RATED</span> DISHES</h2>
							</div>
						  </div>
						 
				<div class="row">
						<div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
							<!-- Block2 -->
							 <?php 


									if(isset($dishes) )
									{
										foreach ($dishes as  $value) {

											
?>
							<div class="block2 ">
								<div class="wrap-pic-w pos-relative ">
									
									<img src="<?=base_url('assets/images/'.$value->dish_image)?>" alt="IMG-PRODUCT" class="top_dishes_images_properties">

									<div class="block2-overlay trans-0-4 top_dishes_images_properties">

										<div class="block2-btn-addcart w-size1 trans-0-4 ">
											<!-- Button -->
											<button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
												ORDER NOW
											</button>
										</div>
									</div>
								</div>
								<div  class="text-center block2-name dis-block s-text3 p-b-5 text-primary dish-custom">
										<div class="dish1">
											<?php print_r($value->dish_title); ?></div>
										<div class="dish-price">RS.<?php print_r($value->regular_price); ?></div>
									</div>
								
									<!-- <a href="product-detail.html" class="text-center block2-name dis-block s-text3 p-b-5 text-primary carousel_font font-weight-bold">
										CHICKEN KARHAI
									</a> -->
								
							</div>
<?php
}
}
?>
						</div>
  
					</div>

					<div class="seemore-btn"><a href="#" class="seemore-btn">see more</a></div>

				</div>

		</section>

		<!-- ENDS DISHES GALLERY -->

		<!-- BEGINS CHEF CAROUSEL 2 -->
		<section class="newproduct bgwhite p-t-45 p-b-105 chef_container">
				<div class="container">
						<div class="text-center mb-6 up_down_spacer">
								<div class="block-heading-1 ">
								  <h2><span>TOP RATED</span> CHEF</h2>
								</div>
							  </div>
		
					<!-- Slide2 -->
					<div class="wrap-chef2 " onmouseover="stop();" onmousemove="start();">
						<div class="chef2">
		
							<div class="item-slick2 p-l-15 p-r-15">
								<!-- Block2 -->
								<div class="block2">
									<div class="wrap-pic-w">
										<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
									</div>
		
									<div class="block2-txt p-t-20">
										<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
											Thoma Balfor
										</a>
									</div>
								</div>
							</div>
							<div class="item-slick2 p-l-15 p-r-15">
									<!-- Block2 -->
									<div class="block2">
										<div class="wrap-pic-w">
											<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
										</div>
			
										<div class="block2-txt p-t-20">
											<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
												Thoma Balfor
											</a>
										</div>
									</div>
								</div>
								<div class="item-slick2 p-l-15 p-r-15">
										<!-- Block2 -->
										<div class="block2">
											<div class="wrap-pic-w">
												<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
											</div>
				
											<div class="block2-txt p-t-20">
												<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
													Thoma Balfor
												</a>
											</div>
										</div>
									</div>
									<div class="item-slick2 p-l-15 p-r-15">
											<!-- Block2 -->
											<div class="block2">
												<div class="wrap-pic-w">
													<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
												</div>
					
												<div class="block2-txt p-t-20">
													<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
														Thoma Balfor
													</a>
												</div>
											</div>
										</div>
										<div class="item-slick2 p-l-15 p-r-15">
												<!-- Block2 -->
												<div class="block2">
													<div class="wrap-pic-w">
														<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
													</div>
						
													<div class="block2-txt p-t-20">
														<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
															Thoma Balfor
														</a>
													</div>
												</div>
											</div>
											<div class="item-slick2 p-l-15 p-r-15">
													<!-- Block2 -->
													<div class="block2">
														<div class="wrap-pic-w">
															<img src="<?=base_url('userassets/images/site/image.jpg')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
														</div>
							
														<div class="block2-txt p-t-20">
															<a href="product-detail.html" class="text-center carousel_font font-weight-bold text-default block2-name dis-block s-text3 p-b-5">
																Thoma Balfor
															</a>
														</div>
													</div>
												</div>
						</div>
					</div>
		<div class="seemore-btn"><a href="#" class="seemore-btn">see more</a></div>
				</div>
			</section>
		<!-- ENDS CHEF CAROUSEL 2 -->
		<!-- BEGINS CLIENTS CAROUSEL -->
		<section class="newproduct bgwhite">
			<div class="container">
					<div class="text-center mb-6 up_down_spacer">
							<div class="block-heading-1 ">
							  <h2><span>OUR CUSTOMER</span> FEEDBACK</h2>
							</div>
						  </div>
	
				<!-- Slide2 -->
				<div class="wrap-clients " onmouseover="stop();" onmousemove="start();">
					<div class="clients">
	
						
										<div class="item-slick2 p-l-15 p-r-15">
												<div class="row ">
													<div class="container text-center my-3">
												  <div class="row mx-auto my-auto">
													  <div id="chefcarousel" class="carousel slide w-100" data-ride="carousel">
														  <div class="carousel-inner customer-feedback" role="listbox">
																		
																
																	<div class="row">
																		<div class="col-12">
																						<img src="<?=base_url('userassets/images/person_1.jpg')?>" class="client_images align-middle pull-center algin-center img-thumbnail pull-center">
																					<div class="custom-feedback">
																						<blockquote class="blockquote text-center pull-right1">
																								<p class="mb-0 align-middle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.consectetur adipiscing elit. Integer posuere erat a ante.</p>
																								<footer class="custom-footer">
																								<div class="custom-title">			
																									John Doe
																								</div>
																								<div class="custom-des" title="Source Title"> - Marketing Manager
																								</div>
																							</footer>
																							  </blockquote>
																			</div>	
										
																		</div>
																   </div>
															
															 </div>
															 
															 
															 
													  </div>
												  </div>
											  </div>
												  </div>
															</div>

										<div class="item-slick2 p-l-15 p-r-15">
												<div class="row ">
													<div class="container text-center my-3">
												  <div class="row mx-auto my-auto">
													  <div id="chefcarousel" class="carousel slide w-100" data-ride="carousel">
														  <div class="carousel-inner customer-feedback" role="listbox">
																		
																
																	<div class="row">
																		<div class="col-12">
																						<img src="<?=base_url('userassets/images/person_1.jpg')?>" class="client_images align-middle pull-center algin-center img-thumbnail pull-center">
																					<div class="custom-feedback">
																						<blockquote class="blockquote text-center pull-right1">
																								<p class="mb-0 align-middle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.consectetur adipiscing elit. Integer posuere erat a ante.</p>
																								<footer class="custom-footer">
																								<div class="custom-title">			
																									John Doe
																								</div>
																								<div class="custom-des" title="Source Title"> - Marketing Manager
																								</div>
																							</footer>
																							  </blockquote>
																			</div>	
										
																		</div>
																   </div>
															
															 </div>
															 
															 
															 
													  </div>
												  </div>
											  </div>
												  </div>
															</div>
				</div>

				</div>
			</div>
			<div class="seemore-btn"><a href="#" class="seemore-btn">see more</a></div>
		</div>
		</section>
		<div class="up_down_spacer"></div>
		<!-- ENDS CLIENTS CAROUSEL -->
		<!-- BEGINS PARTNERS CAROUSEL -->
		<section class="newproduct bgwhite p-t-45 p-b-105 chef_container">
				<div class="container">
						<div class="text-center mb-6 up_down_spacer">
								<div class="block-heading-1 ">
								  <h2><span>AFFILAITE </span> PARTNERS</h2>
								</div>
							  </div>
		
					<!-- Slide2 -->
					<div class="wrap-partners" onmouseover="stop();" onmousemove="start();">
						<div class="partners">
		
							<div class="item-slick2 p-l-15 p-r-15">
								<!-- Block2 -->
								<div class="block3">
									<div class="wrap-pic-w">
										<img src="<?=base_url('userassets/images/icons/1.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
									</div>
								</div>
							</div>
							<div class="item-slick2 p-l-15 p-r-15">
									<!-- Block2 -->
									<div class="block3">
										<div class="wrap-pic-w">
											<img src="<?=base_url('userassets/images/icons/2.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
										</div>
									</div>
								</div>
								<div class="item-slick2 p-l-15 p-r-15">
										<!-- Block2 -->
										<div class="block3">
											<div class="wrap-pic-w">
												<img src="<?=base_url('userassets/images/icons/3.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
											</div>
										</div>
									</div>
									<div class="item-slick2 p-l-15 p-r-15">
											<!-- Block2 -->
											<div class="block3">
												<div class="wrap-pic-w">
													<img src="<?=base_url('userassets/images/icons/4.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
												</div>
											</div>
										</div>
										<div class="item-slick p-l-15 p-r-15">
												<!-- Block2 -->
												<div class="block3">
													<div class="wrap-pic-w">
														<img src="<?=base_url('userassets/images/icons/5.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
													</div>
												</div>
											</div>
											<div class="item-slick2 p-l-15 p-r-15">
													<!-- Block2 -->
													<div class="block3">
														<div class="wrap-pic-w">
															<img src="<?=base_url('userassets/images/icons/6.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
														</div>
													</div>
												</div>
												<div class="item-slick2 p-l-15 p-r-15">
														<!-- Block2 -->
														<div class="block3">
															<div class="wrap-pic-w">
																<img src="<?=base_url('userassets/images/icons/8.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
															</div>
														</div>
													</div>
													<div class="item-slick2 p-l-15 p-r-15">
															<!-- Block2 -->
															<div class="block3">
																<div class="wrap-pic-w">
																	<img src="<?=base_url('userassets/images/icons/1.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
																</div>
															</div>
														</div>
														<div class="item-slick p-l-15 p-r-15">
															<!-- Block2 -->
															<div class="block3">
																<div class="wrap-pic-w">
																	<img src="<?=base_url('userassets/images/icons/2.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
																</div>
															</div>
														</div>
														<div class="item-slick p-l-15 p-r-15">
															<!-- Block2 -->
															<div class="block3">
																<div class="wrap-pic-w">
																	<img src="<?=base_url('userassets/images/icons/3.png')?>" alt="IMG-PRODUCT" class="dishes_images_properties img_zoomer">
																</div>
															</div>
														</div>
									
						</div>
					</div>
		
				</div>
			</section>
		<!-- ENDS PARTNERS CAROUSEL -->
	<!-- Footer -->
	<footer class="p-t-45 p-b-43 p-l-45 p-r-45" style="background-image: url('<?=base_url('userassets/images/footer_img5.jpg')?>'); background-size: cover">
		<div class="flex-w p-b-90">
			<div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					<img src="<?=base_url('userassets/images/logo.png')?>" class="footer-logo" alt="IMG-LOGO">
				</h4>
				<div>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-facebook"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-instagram"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-pinterest-p"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-snapchat-ghost"></a>
						<a href="#" class="fs-18 color0 p-r-20 fa fa-youtube-play"></a>
					</div>

				<div>
					<p class="s-text7 w-size27">
						Food orders and tables booking web app
						Food orders and tables booking web app
						Food orders and tables booking web app


						
					</p>
					<a href="#"><i class="fa fa-arrow-right"></i> Read More </a>

					
				</div>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Categories
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							FastFood
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Traditional
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
								Deserts
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Extras
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Links
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Search
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							About Us
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Contact Us
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Returns
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
				<h4 class="s-text12 p-b-30">
					Help
				</h4>

				<ul>
					<li class="p-b-9">
						<a href="#" class="s-text7">
							Track Order
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Returns
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							Location
						</a>
					</li>

					<li class="p-b-9">
						<a href="#" class="s-text7">
							FAQs
						</a>
					</li>
				</ul>
			</div>

			<div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
				<h4 class="s-text12 p-b-30">
					Contact Us
				</h4>
				Our Location? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
			</div>
		</div>
		<!-- BEGIN LOGIN MODAL -->
		<div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<!-- CUSTOMER_LOGIN -->
		<div class="modal-header modal-heading-sp">
			<div class="form-group col-lg-6 modal-heading"><div class="p-b-7 p-t-7 text-left">LOGIN HERE</div></div>
			<div class="form-group col-lg-6">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
	  </div>		
		<div class="modal-body text-center pull-center modal-spacing" id="login_form">				
					<form action="UserController/login_users" method="post">
						<div class="form-row">
							<div class="form-group col-md-12">
									<select name="role" class="form-control" required="">
											<option value="" selected="selected">Please Choose Role</option>
											<option value="Login as Chef">Login as Chef</option>
											<option value="Login as Customer">Login as Customer</option>
									</select>
							</div>

						  <div class="form-group col-md-12">
							<input type="email" class="form-control" name="email" placeholder="Email">
						  </div>
						  <div class="form-group col-md-12">
							<input type="password" class="form-control" name="password" placeholder="Password">
						  </div>
						  <div class="form-group col-md-4 left-text">
								<a href="#" id="open_forgot_form" >Forgot Password</a>
							  </div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
							  <button type="submit" class="login_btn_color btn-lg btn-block">Log in</button>
							</div>
						  </div>
						</form>		
				</div>
				<!-- END CUSTOMER LOGIN -->
				<!-- RESET PASSWORD -->
				<div class="modal-body text-center pull-center" id="forgot_form">
						<p>Enter your email address</p>
						<form>
							<div class="form-row">
							  <div class="form-group col-md-12">
								<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
							  </div>
							  <div class="form-group col-md-12">
									<p>Already have an account? <a href="#" id="open_login_form"> Login</a> </p>
								  </div>
							</div>
							<div class="form-group row">
								<div class="col-md-12">
								  <button type="submit" class="login_btn_color btn-lg btn-block">Submit</button>
								</div>
							  </div>
							</form>			
					</div>
		<div class="modal-footer">
		  
		</div>
	  </div>
	</div>
  </div>
		<!-- END LOGIN MODAL -->
	  <!-- BEGIN REGISTER MODAL -->

	  <div class="modal fade" id="registermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			<div style="margin-bottom: 10px;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>			  
					<!-- CUSTOMER REGISTERATION -->
					<div class="modal-body text-center pull-center modal-spacing" id="customer_register_form">  				
				
						<div class="form-row">
							<div class="form-group col-md-7">

								<div id="notification"></div>

								<h5 class="p-b-7 p-t-7 text-left">CUSTOMER REGISTRATION</h5>
							</div>							
							<div class="form-group col-md-5">
							<button type="button" class="login_btn_color btn-block p-b-3 p-l-3" id="open_chef_register_form">BECOME A CHEF</button>
						</div>
						  
						  <div class="form-group col-md-12">
							<input type="text" class="form-control" id="cus_fname" placeholder="First Name">
						  </div>
						  <div class="form-group col-md-12">
							<input type="text" class="form-control" id="cus_lname" placeholder="last Name">
						  </div>
						  <div class="form-group col-md-12">
							<input type="email" class="form-control" id="cus_email" placeholder="Email" required="">
						  </div>
						  <div class="form-group col-md-12">
							<input type="number" class="form-control" id="cus_mob_num" placeholder="Mobile No.">
						  </div>						  
						  <div class="form-group col-md-12">
							<input type="password" class="form-control" id="cus_password" placeholder="Password">
						  </div>
						  <div class="form-group col-md-12">
							<input type="password" class="form-control" id="cus_c_password" placeholder="Confirm Password">
						  </div>						  
							<!--  <div class="form-group col-md-12">
									<a href="#" id="open_forgot_form" >Forgot Password</a>
								  </div> -->
						</div>
						<div class="form-group row">
							<div class="col-md-12">
							  <input id="insert" type="submit" class="login_btn_color btn-lg btn-block p-b-3 p-l-3" value="Register">
							</div>
						  </div>
							
				</div>

				<script type="text/javascript">
				  $(document).ready(function(){
				    $('#insert').click(function(){
				      var cus_fname = $('#cus_fname').val();
				      var cus_lname = $('#cus_lname').val();
				      var cus_email = $('#cus_email').val();
				      var cus_mob_num = $('#cus_mob_num').val();
				      var cus_password = $('#cus_password').val();
				      var cus_c_password = $('#cus_c_password').val();

				      $.ajax({
				        url: '<?=base_url("UserController/cus_insertion")?>',
				        method: 'POST',
				        data: {
				          cus_fname:cus_fname,
				          cus_lname:cus_lname,
				          cus_email:cus_email,
				          cus_mob_num:cus_mob_num,
				          cus_password:cus_password,
				          cus_c_password:cus_c_password
				        },
				        success:function(data){
				          $('#notification').html(data);
				        }
				      })
				    });
				  });

				</script>

				<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
				<!-- END CUSTOMER REGISTERATION -->



				<!-- CHEF REGISTERATION -->
				<div class="modal-body text-center pull-center modal-spacing" id="chef_register_form">		
						
							<div class="form-row">
								<div class="form-group col-md-6">

									<div id="notification2"></div>

									<h5 class="p-b-7 p-t-7 text-left">CHEF REGISTRATION</h5>
								</div>								
								<div class="form-group col-md-6">
								<button type="button" class="login_btn_color btn-block" id="open_customer_register_form">BECOME A CUSTOMER</button>
							  </div>
							  <div class="form-group col-md-12">
								<input type="text" class="form-control" value="" id="chef_fname" placeholder="First Name">
							  </div>
							  <div class="form-group col-md-12">
								<input type="text" class="form-control" id="chef_lname" placeholder="last Name">
							  </div>
							  <div class="form-group col-md-12">
								<input type="email" class="form-control" id="chef_email" placeholder="Email" required="">
							  </div>	
							  <div class="form-group col-md-12">
								<input type="number" class="form-control" id="chef_mob_num" placeholder="Mobile No.">
							  </div>							  						  
							  <div class="form-group col-md-12">
								<input type="text" class="form-control" id="chef_bname" placeholder="Business Name">
							  </div>
							  <div class="form-group col-md-12">
								<input type="number" class="form-control" id="chef_l_num" placeholder="Landline No.">
							  </div>							  
							  <div class="form-group col-md-12">
								<input type="password" class="form-control" id="chef_password" placeholder="Password">
							  </div>
							  <div class="form-group col-md-12">
								<input type="password" class="form-control" id="chef_c_password" placeholder="Confirm Password">
							  </div>							  
								<!--  <div class="form-group col-md-12">
										<a href="#" id="open_forgot_form" >Forgot Password</a>
									  </div> -->
							</div>
							<div class="form-group row">
							<div class="col-md-12">
							  <input id="insert2" type="submit" class="login_btn_color btn-lg btn-block p-b-3 p-l-3" value="Register">
							</div>
						  </div>		
					</div>
					<script type="text/javascript">
					  $(document).ready(function(){
					    $('#insert2').click(function(){
					      var chef_fname = $('#chef_fname').val();
					      var chef_lname = $('#chef_lname').val();
					      var chef_email = $('#chef_email').val();
					      var chef_mob_num = $('#chef_mob_num').val();
					      var chef_bname = $('#chef_bname').val();
					      var chef_l_num = $('#chef_l_num').val();
					      var chef_password = $('#chef_password').val();
					      var chef_c_password = $('#chef_c_password').val();

					      $.ajax({
					        url: '<?=base_url("UserController/chef_insertion")?>',
					        method: 'POST',
					        data: {
					          chef_fname:chef_fname,
					          chef_lname:chef_lname,
					          chef_email:chef_email,
					          chef_mob_num:chef_mob_num,
					          chef_bname:chef_bname,
					          chef_l_num:chef_l_num,
					          chef_password:chef_password,
					          chef_c_password:chef_c_password
					        },
					        success:function(data){
					          $('#notification2').html(data);
					        }
					      })
					    });
					  });
					</script>
					
					<!-- END CHEF REGISTERATION -->
					
			<div class="modal-footer">
			  
			</div>
		  </div>
		</div>
	  </div>
	  <!-- END LOGIN MODAL -->
	<!-- cards icons	 -->
	  <!-- <div class="t-center p-l-15 p-r-15"> -->
			<!-- <a href="#">
				<img class="h-size2" src="images/icons/paypal.png" alt="IMG-PAYPAL">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/visa.png" alt="IMG-VISA">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/mastercard.png" alt="IMG-MASTERCARD">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/express.png" alt="IMG-EXPRESS">
			</a>

			<a href="#">
				<img class="h-size2" src="images/icons/discover.png" alt="IMG-DISCOVER">
			</a> -->
		<!-- </div> -->
	<!-- ends cards icons -->
	
	</footer>
			<div class="t-center p-t-10 p-b-10 color0 bg-primary">
				<div class="text-center mb-6 ">
					  <p><span class="font-weight-bold text text-light">POWERED BY FIRMSOLS</span></p>
					</div>
				
			</div>


	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/animsition/js/animsition.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/popper.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('userassets/vendor/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/select2/select2.min.js')?>"></script>
	<script type="text/javascript">
		$(".selection-1").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/slick/slick.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('userassets/js/slick-custom.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/countdowntime/countdowntime.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/lightbox2/js/lightbox.min.js')?>"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="<?=base_url('userassets/vendor/sweetalert/sweetalert.min.js')?>"></script>
	<script type="text/javascript">
		$('.block2-btn-addcart').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

		$('.block2-btn-addwishlist').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");
			});
		});
	</script>

<!--===============================================================================================-->
	<script src="<?=base_url('userassets/js/home-main.js')?>"></script>
</body>
</html>
