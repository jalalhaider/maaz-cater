<div id="all_dishes">
                                <div class="text-center mb-6 up_spacer" id="all_dishes_heading">
                                    <div class="sub-h3">
                                        <h3><span>All</span> DISHES</h3>
                                    </div>
                                </div>
                                <div class="show_data">
                                <div class="table-responsive">
                                    <table id="mytable" class="table table-bordred table-striped">
        
                                        <thead>
        
                                            <tr>
                                                <th>Dish Title</th>
                                                <th>Main Category</th>
                                                <th>Sub Category</th>
                                                <th>Veg/Non-Veg</th>
                                                <th>Time</th>
                                                <th>Price</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                        <?php foreach($dishes as $value){ ?>
                                            <tr>
                                                
                                            <td id="dish_title"><?php echo $value->dish_title;?></td>
                                            <td id="dish_type"><?php echo $value->dish_type;?></td>
                                            <td id="religious"><?php echo $value->religious;?></td>
                                            <td id="meat"><?php echo $value->meat;?></td>
                                            <td id="time"><?php echo $value->time;?></td>
                                            <td id="regular_price"><?php echo $value->regular_price;?></td>
                                            <td>
                                                    
                                            <p data-placement="top" data-toggle="tooltip" title="Action">


<?php echo anchor('UserController/edit_dish/'.$value->id, ' ',['class'=>'fa fa-eye']);?>
<?php echo anchor('UserController/dlt_dish/'.$value->id, ' ',['class'=>'fa fa-trash']);?>

<a href="#home-section" class="fa fa-eye" data-toggle="modal" data-target="#dish_modal"></a>


                                                    </p>
                                                </td>
                                            </tr>
                                            
                                        <?php } ?>
        
                                        </tbody>
        
                                    </table>
        
                                </div>
                              </div>
                            </div>