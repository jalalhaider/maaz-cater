/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.13-MariaDB : Database - cater_local
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bank` */

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `country` varchar(55) NOT NULL,
  `state` varchar(55) NOT NULL,
  `city` varchar(55) NOT NULL,
  `bank_name` varchar(55) NOT NULL,
  `account_title` varchar(55) NOT NULL,
  `account_number` int(55) NOT NULL,
  `iban_number` int(55) NOT NULL,
  `branch_code` varchar(55) NOT NULL,
  `branch_name` varchar(55) NOT NULL,
  `checked` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `bank` */

insert  into `bank`(`id`,`country`,`state`,`city`,`bank_name`,`account_title`,`account_number`,`iban_number`,`branch_code`,`branch_name`,`checked`) values (8,'Pakistan','Azad Kashmir','lahore','UBL','sdsd',333,33333,'222','dddd',0);
insert  into `bank`(`id`,`country`,`state`,`city`,`bank_name`,`account_title`,`account_number`,`iban_number`,`branch_code`,`branch_name`,`checked`) values (9,'Pakistan','Bahawalpur','Karachi','NBP','sdsd',132,33333,'222','dddd',0);

/*Table structure for table `category_1` */

DROP TABLE IF EXISTS `category_1`;

CREATE TABLE `category_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(233) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `category_1` */

insert  into `category_1`(`id`,`Name`) values (18,'American');
insert  into `category_1`(`id`,`Name`) values (19,'Thai');
insert  into `category_1`(`id`,`Name`) values (20,'Indian');
insert  into `category_1`(`id`,`Name`) values (21,'Pakistani');
insert  into `category_1`(`id`,`Name`) values (22,'Mexican');
insert  into `category_1`(`id`,`Name`) values (23,'Chinese');
insert  into `category_1`(`id`,`Name`) values (24,'Greek');
insert  into `category_1`(`id`,`Name`) values (25,'Mediterranean');
insert  into `category_1`(`id`,`Name`) values (26,'Italian ');
insert  into `category_1`(`id`,`Name`) values (27,'Latin American');
insert  into `category_1`(`id`,`Name`) values (28,'African');
insert  into `category_1`(`id`,`Name`) values (29,'Japanese / Sushi');

/*Table structure for table `category_2` */

DROP TABLE IF EXISTS `category_2`;

CREATE TABLE `category_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(21) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `category_2` */

insert  into `category_2`(`id`,`Name`) values (11,'Halal');
insert  into `category_2`(`id`,`Name`) values (12,'Kosher');

/*Table structure for table `category_3` */

DROP TABLE IF EXISTS `category_3`;

CREATE TABLE `category_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(21) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `category_3` */

insert  into `category_3`(`id`,`Name`) values (9,'Non veg');
insert  into `category_3`(`id`,`Name`) values (10,'Veg');
insert  into `category_3`(`id`,`Name`) values (11,'Vegan ');

/*Table structure for table `category_4` */

DROP TABLE IF EXISTS `category_4`;

CREATE TABLE `category_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(21) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `category_4` */

insert  into `category_4`(`id`,`Name`) values (6,'Fast food');
insert  into `category_4`(`id`,`Name`) values (7,'Breakfast');
insert  into `category_4`(`id`,`Name`) values (8,'Lunch / Dinner');

/*Table structure for table `category_5` */

DROP TABLE IF EXISTS `category_5`;

CREATE TABLE `category_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(21) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `category_5` */

insert  into `category_5`(`id`,`Name`) values (7,'Appetizers');
insert  into `category_5`(`id`,`Name`) values (8,'Soups and salad');
insert  into `category_5`(`id`,`Name`) values (9,'Main dishes');
insert  into `category_5`(`id`,`Name`) values (11,'desserts');
insert  into `category_5`(`id`,`Name`) values (12,'Drinks');

/*Table structure for table `category_6` */

DROP TABLE IF EXISTS `category_6`;

CREATE TABLE `category_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `category_6` */

insert  into `category_6`(`id`,`Name`) values (1,'Italian');
insert  into `category_6`(`id`,`Name`) values (2,'Chinese');
insert  into `category_6`(`id`,`Name`) values (3,'Indian');
insert  into `category_6`(`id`,`Name`) values (4,'Pakistani');

/*Table structure for table `chef` */

DROP TABLE IF EXISTS `chef`;

CREATE TABLE `chef` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `fname` varchar(55) NOT NULL,
  `lname` varchar(55) NOT NULL,
  `bname` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `chef_image` varchar(200) NOT NULL,
  `l_num` int(55) NOT NULL,
  `mob_num` int(55) NOT NULL,
  `password` varchar(55) NOT NULL,
  `cpassword` varchar(55) NOT NULL,
  `cnic` int(55) NOT NULL,
  `chef_cnic_fpic` tinytext NOT NULL,
  `chef_cnic_bpic` tinytext NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `chef` */

insert  into `chef`(`id`,`fname`,`lname`,`bname`,`email`,`chef_image`,`l_num`,`mob_num`,`password`,`cpassword`,`cnic`,`chef_cnic_fpic`,`chef_cnic_bpic`,`status`) values (1,'alex','alex','djksfj','alex@gmail.com','',0,0,'alex','alex',0,'','',1);

/*Table structure for table `chef_dishes` */

DROP TABLE IF EXISTS `chef_dishes`;

CREATE TABLE `chef_dishes` (
  `chef_id` int(11) NOT NULL,
  `dish_id` int(11) NOT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`chef_id`,`dish_id`),
  KEY `dish_fk_02` (`dish_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `chef_dishes` */

insert  into `chef_dishes`(`chef_id`,`dish_id`,`is_active`) values (1,33,1);
insert  into `chef_dishes`(`chef_id`,`dish_id`,`is_active`) values (1,34,1);
insert  into `chef_dishes`(`chef_id`,`dish_id`,`is_active`) values (1,35,1);

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `country` varchar(55) NOT NULL,
  `city` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `cities` */

insert  into `cities`(`id`,`country`,`city`) values (4,'Pakistan','lahore');
insert  into `cities`(`id`,`country`,`city`) values (5,'India','Mumbai');
insert  into `cities`(`id`,`country`,`city`) values (6,'Pakistan','Karachi');

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `Country` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert  into `countries`(`id`,`Country`) values (7,'Pakistan');
insert  into `countries`(`id`,`Country`) values (8,'India');
insert  into `countries`(`id`,`Country`) values (9,'Iran');
insert  into `countries`(`id`,`Country`) values (10,'Bangladesh');
insert  into `countries`(`id`,`Country`) values (11,'venezuela');

/*Table structure for table `coupons` */

DROP TABLE IF EXISTS `coupons`;

CREATE TABLE `coupons` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `coupon_title` varchar(55) NOT NULL,
  `coupon_code` varchar(55) NOT NULL,
  `activation_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `discount` varchar(55) NOT NULL,
  `coupon_type` varchar(55) NOT NULL,
  `dish_title` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `coupons` */

insert  into `coupons`(`id`,`coupon_title`,`coupon_code`,`activation_date`,`expiry_date`,`discount`,`coupon_type`,`dish_title`) values (1,'Eid coupon','7y6r5','2020-02-08','2020-02-10','35%','Single Dish','Zinger Burger');
insert  into `coupons`(`id`,`coupon_title`,`coupon_code`,`activation_date`,`expiry_date`,`discount`,`coupon_type`,`dish_title`) values (11,'new year','jkjk','2020-02-08','2020-02-23','75%','Single Dish','chicken corn soup');
insert  into `coupons`(`id`,`coupon_title`,`coupon_code`,`activation_date`,`expiry_date`,`discount`,`coupon_type`,`dish_title`) values (12,'zxz','0314','2020-02-28','2020-02-29','85%','Single Dish','Zinger Burger');
insert  into `coupons`(`id`,`coupon_title`,`coupon_code`,`activation_date`,`expiry_date`,`discount`,`coupon_type`,`dish_title`) values (13,'50%OFF','EAT50','2020-02-10','2020-02-15','50%','Single Dish','kachi biryani');

/*Table structure for table `customer_dishes` */

DROP TABLE IF EXISTS `customer_dishes`;

CREATE TABLE `customer_dishes` (
  `customer_id` int(11) NOT NULL,
  `dish_id` int(11) NOT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`customer_id`,`dish_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer_dishes` */

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `fname` varchar(55) NOT NULL,
  `lname` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `mob_num` varchar(55) NOT NULL,
  `password` varchar(55) NOT NULL,
  `cpassword` varchar(55) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `bname` int(100) NOT NULL,
  `l_num` int(111) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

/*Data for the table `customers` */

insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (1,'saboor','ahmed','saboor@gmail.com','777','saboor','saboor',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (6,'fareed','shah','fareed@yahoo.com','2121','fareed','fareed',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (7,'naveed','ahmed','naveed@yahoo.com','23787','naveed','naveed',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (8,'saleem','soomro','saleem@yahoo.com','2222','78445','3798',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (10,'Hussain','Ali','hussain@gmail.com','2637','dsd','hdjs',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (11,'ggg','gggg','gggg','77777','8hjh','hjhkhkj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (12,'uu','uuu','uu','8778','uuuuu','uuuuuu',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (13,'','','','','','',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (14,'','','','','','',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (15,'ashdkj','hdajkdh','2dha','7879','hdksajhd','hkdjashdj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (16,'hh','iii','yyyy@yahoo.com','3333','ggggg','gggg',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (17,'yyy','yyyy','yyyy','787878','ggggg','gggg',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (18,'yyy','yyyy','yyyy','787878','ggggg','gggg',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (19,'ksahdkj','hhdaskjh','syedibrahimshah428@gmail.com','2378','sdyhskjadh','dhkajsd',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (20,'ksahdkj','hhdaskjh','syedibrahimshah428@gmail.com','2378','sdyhskjadh','dhkajsd',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (21,'ksahdkj','hhdaskjh','syedibrahimshah428@gmail.com','2378','sdyhskjadh','dhkajsd',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (22,'sarim','khan','sarim@yahoo.com','666','wwww','wwwww',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (23,'Sumaim','Ahmed','saleem123@yahoo.com','2222','123','123',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (24,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (25,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (26,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (27,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (28,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (29,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (30,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (31,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (32,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (33,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (34,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (35,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (36,'asjkhd','kjhdakjh','dhaskjh@yahoo.com','8908','9daskdh','hkasjhdkj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (37,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (38,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (39,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (40,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (41,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (42,'sdkjas','jdlksa','jdlkas','877','hkjh','hj',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (43,'sajkdhJHK','hdkjsah','hjshdk@yahoo.com','8797','hskjah','hksjqhk',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (44,'sajkdhJHK','hdkjsah','hjshdk@yahoo.com','8797','hskjah','hksjqhk',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (45,'sajkdhJHK','hdkjsah','hjshdk@yahoo.com','8797','hskjah','hksjqhk',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (46,'sajkdhJHK','hdkjsah','hjshdk@yahoo.com','8797','hskjah','hksjqhk',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (47,'sajkdhJHK','hdkjsah','hjshdk@yahoo.com','8797','hskjah','hksjqhk',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (48,'dsakjjh','kjhdaksjhd','hdaksjdhakj@gmail.com','7987','dhkja','hdkaskjdkjas',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (49,'rahman','khan','rahman@yahoo.com','7777','rahman','raman',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (50,'shayan','khan','shayan@yahoo.com','9999','shayan','shayan',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (51,'saleem','jhandeer','saleem@yahoo.com','0320320320','saleem','saleem',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (52,'arham','ahmed','arham@yahoo.com','8238283','arham','arham',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (53,'','','','','','',0,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (54,'ahmed','shah','ahmed@gmail.com','8888','111','111',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (55,'dilshad','ahmed','dilshadcs49@gmail.com','03054821131','12345','12345',1,0,0);
insert  into `customers`(`id`,`fname`,`lname`,`email`,`mob_num`,`password`,`cpassword`,`status`,`bname`,`l_num`) values (56,'hassan','sultan','hassan@gmail.com','923462675951','123','123',1,0,0);

/*Table structure for table `data` */

DROP TABLE IF EXISTS `data`;

CREATE TABLE `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(35) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `data` */

insert  into `data`(`id`,`Email`,`Password`,`status`) values (1,'ali@gmail.com','ali',0);

/*Table structure for table `dish_type` */

DROP TABLE IF EXISTS `dish_type`;

CREATE TABLE `dish_type` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `time` varchar(55) NOT NULL,
  `eating` varchar(55) NOT NULL,
  `dish_type` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `dish_type` */

insert  into `dish_type`(`id`,`time`,`eating`,`dish_type`) values (8,'Fast food','Main dishes','Burger');
insert  into `dish_type`(`id`,`time`,`eating`,`dish_type`) values (10,'Breakfast','desserts','kheer');
insert  into `dish_type`(`id`,`time`,`eating`,`dish_type`) values (12,'Fast food','Main dishes','chicken burger');
insert  into `dish_type`(`id`,`time`,`eating`,`dish_type`) values (13,'Lunch / Dinner','Main dishes','biryani');
insert  into `dish_type`(`id`,`time`,`eating`,`dish_type`) values (14,'Breakfast','desserts','kheer');

/*Table structure for table `dishes` */

DROP TABLE IF EXISTS `dishes`;

CREATE TABLE `dishes` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `region` varchar(55) NOT NULL,
  `religious` varchar(55) NOT NULL,
  `meat` varchar(55) NOT NULL,
  `time` varchar(55) NOT NULL,
  `eating` varchar(55) NOT NULL,
  `dish_type` varchar(55) NOT NULL,
  `dish_title` varchar(55) NOT NULL,
  `regular_price` int(55) NOT NULL,
  `sale_price` int(55) NOT NULL,
  `calories` text NOT NULL,
  `used_ingredients` text NOT NULL,
  `coupon` varchar(55) NOT NULL DEFAULT 'not applied',
  `dish_image` varchar(200) NOT NULL,
  `ingredients` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `dishes` */

insert  into `dishes`(`id`,`region`,`religious`,`meat`,`time`,`eating`,`dish_type`,`dish_title`,`regular_price`,`sale_price`,`calories`,`used_ingredients`,`coupon`,`dish_image`,`ingredients`) values (33,'American','Halal','Non veg','Fast food','Main dishes','Burger','chiken karhai',2222,6777,'31 to 40','biryani','not applied','tsdish1.JPG','biryani');
insert  into `dishes`(`id`,`region`,`religious`,`meat`,`time`,`eating`,`dish_type`,`dish_title`,`regular_price`,`sale_price`,`calories`,`used_ingredients`,`coupon`,`dish_image`,`ingredients`) values (34,'Pakistani','Halal','Non veg','Fast food','Main dishes','chicken burger','chiken karhai',2222,3333,'91 to 100','pizza','not applied','dish3.JPG','chiken masala');
insert  into `dishes`(`id`,`region`,`religious`,`meat`,`time`,`eating`,`dish_type`,`dish_title`,`regular_price`,`sale_price`,`calories`,`used_ingredients`,`coupon`,`dish_image`,`ingredients`) values (35,'Chinese','Halal','Meat','Time','Eatings','biryani','chiken karhai',332323,123,'91 to 100','','not applied','dish1.JPG','fish masala');
insert  into `dishes`(`id`,`region`,`religious`,`meat`,`time`,`eating`,`dish_type`,`dish_title`,`regular_price`,`sale_price`,`calories`,`used_ingredients`,`coupon`,`dish_image`,`ingredients`) values (36,'Italian ','Religious','Meat','Time','Eatings','Food Type','654',6754,2222,'Calories','biryani','not applied','dish11.JPG','brown onion');
insert  into `dishes`(`id`,`region`,`religious`,`meat`,`time`,`eating`,`dish_type`,`dish_title`,`regular_price`,`sale_price`,`calories`,`used_ingredients`,`coupon`,`dish_image`,`ingredients`) values (37,'Select Region','Halal','Veg','Time','Eatings','Food Type','burger',230,210,'31 to 40','burger','not applied','dish2.JPG','termeric');
insert  into `dishes`(`id`,`region`,`religious`,`meat`,`time`,`eating`,`dish_type`,`dish_title`,`regular_price`,`sale_price`,`calories`,`used_ingredients`,`coupon`,`dish_image`,`ingredients`) values (38,'Latin American','Halal','Veg','Breakfast','Main dishes','chicken burger','burger',8888,222,'71 to 80','','not applied','dish4.JPG','');
insert  into `dishes`(`id`,`region`,`religious`,`meat`,`time`,`eating`,`dish_type`,`dish_title`,`regular_price`,`sale_price`,`calories`,`used_ingredients`,`coupon`,`dish_image`,`ingredients`) values (39,'Indian','Halal','Non veg','Time','Eatings','Food Type','chiken karhai',6754,210,'1 to 10','burger','not applied','dish21.JPG','');

/*Table structure for table `ingredients` */

DROP TABLE IF EXISTS `ingredients`;

CREATE TABLE `ingredients` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `ingredients` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `ingredients` */

insert  into `ingredients`(`id`,`ingredients`) values (2,'biryani masala');
insert  into `ingredients`(`id`,`ingredients`) values (7,'fish masala');
insert  into `ingredients`(`id`,`ingredients`) values (8,'chicken masala');
insert  into `ingredients`(`id`,`ingredients`) values (9,'brown onion');
insert  into `ingredients`(`id`,`ingredients`) values (10,'turmeric');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `order_id` int(55) NOT NULL,
  `description` text NOT NULL,
  `amount` int(55) NOT NULL,
  `status` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `orders` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `chef_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `permissions` */

insert  into `permissions`(`permission_id`,`chef_id`,`customer_id`,`status`) values (1,1,1,1);
insert  into `permissions`(`permission_id`,`chef_id`,`customer_id`,`status`) values (2,2,2,1);
insert  into `permissions`(`permission_id`,`chef_id`,`customer_id`,`status`) values (3,0,2,1);

/*Table structure for table `stages` */

DROP TABLE IF EXISTS `stages`;

CREATE TABLE `stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `stages` */

insert  into `stages`(`id`,`name`) values (1,'level-1');
insert  into `stages`(`id`,`name`) values (2,'level-2');
insert  into `stages`(`id`,`name`) values (3,'level-3');

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id` int(55) NOT NULL AUTO_INCREMENT,
  `country` varchar(55) NOT NULL,
  `state` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `states` */

insert  into `states`(`id`,`country`,`state`) values (1,'Pakistan','Bahawalpur');
insert  into `states`(`id`,`country`,`state`) values (2,'Pakistan','Azad Kashmir');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
